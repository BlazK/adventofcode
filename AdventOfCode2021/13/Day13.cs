﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AdventOfCode2021
{
  public class Day13
  {
    public static void Solve(int part)
    {
      List<Dot> dots = new List<Dot>();
      List<(string Axis, int Value)> instructions = new List<(string Axis, int Value)>();
      
      using StreamReader sr = new StreamReader("13/13.txt");
      string line;
      bool readingInstructions = false;
      while ((line = sr.ReadLine()) != null)
      {
        if (!readingInstructions && line.Length > 0)
        {
          string[] words = line.Split(",");
          dots.Add(new Dot(dots.Count + 1, int.Parse(words[0]), int.Parse(words[1])));
        }
        else if (line.Length == 0)
        {
          readingInstructions = true;
        }
        else
        {
          string[] words = line.Split(" ")[2].Split("=");
          instructions.Add((words[0], int.Parse(words[1])));
        }
      }

      int sizeY = 0, sizeX = 0;
      
      for (int i = 0; part == 1 ? i < 1 : i < instructions.Count; i++)
      {
        sizeY = dots.Max(x => x.Y);
        sizeX = dots.Max(x => x.X);
        bool foldHorizontal = instructions[i].Axis.Equals("x");
        PerformFold(dots, foldHorizontal, instructions[i].Axis, instructions[i].Value);
      }

      if (part == 1)
      {
        Console.WriteLine($"There are {dots.Count} dots visible."); 
      }
      else
      {
        Draw(dots, sizeY, sizeX);
      }
    }

    private static void Draw(List<Dot> dots, int sizeY, int sizeX)
    {
      for (int i = 0; i <= sizeY; i++)
      {
        for (int j = 0; j <= sizeX; j++)
        {
          if (dots.Any(x => x.Y == i && x.X == j))
          {
            Console.Write("#");
          }
          else
          {
            Console.Write(".");
          }
        }

        Console.WriteLine();
      }
    }

    private static void PerformFold(List<Dot> dots, bool foldHorizontal, string axis, int value)
    {
      List<int> dotsToRemove = new List<int>();
      
      for (int i = 0; i < dots.Count; i++)
      {
        if (foldHorizontal && dots[i].X == value || !foldHorizontal && dots[i].Y == value)
        {
          dotsToRemove.Add(i);
        }
        else if (foldHorizontal && dots[i].X > value)
        {
          int diff = dots[i].X - value;
          int newValue = value - diff;
          dots[i].X = newValue;
          if (dots.Any(x => x.X == newValue && x.Y == dots[i].Y && x.Id != dots[i].Id))
          {
            dotsToRemove.Add(dots[i].Id);
          }
        }
        else if (!foldHorizontal && dots[i].Y > value)
        {
          int diff = dots[i].Y - value;
          int newValue = value - diff;
          dots[i].Y = newValue;
          if (dots.Any(x => x.X == dots[i].X && x.Y == newValue && x.Id != dots[i].Id))
          {
            dotsToRemove.Add(dots[i].Id);
          }
        }
      }

      foreach (int id in dotsToRemove)
      {
        int idx = dots.FindIndex(x => x.Id == id);
        if (idx != -1)
        {
          dots.RemoveAt(idx);
        }
      }
    }

    private class Dot
    {
      public int Id { get; set; }
      public int X { get; set; }
      public int Y { get; set; }

      public Dot(int id, int x, int y)
      {
        Id = id;
        X = x;
        Y = y;
      }
    }
  }
}