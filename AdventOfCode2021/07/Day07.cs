﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AdventOfCode2021
{
  public class Day07
  {
    public static void Solve(int part)
    {
      using StreamReader sr = new StreamReader("07.txt");
      List<int> crabs = sr.ReadLine()?.Split(",").Select(int.Parse).OrderBy(x => x).ToList() ?? new List<int>();

      long minSum = long.MaxValue;
      for (int crab = crabs[0]; crab <= crabs[^1]; crab++)
      {
        long sum = 0;
        foreach (int crab2 in crabs)
        {
          if (crab != crab2)
          {
            if (part == 1)
            {
              sum += Math.Abs(crab - crab2);
            }
            else
            {
              for (int i = Math.Min(crab, crab2), j = 1; i < Math.Max(crab, crab2); i++, j++)
              {
                sum += j;
              } 
            } 
          }
        }

        if (sum < minSum)
        {
          minSum = sum;
        }
      }

      Console.WriteLine($"Minimal fuel consumption is {minSum}");
    }
  }
}