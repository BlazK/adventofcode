﻿using System;
using System.IO;

namespace AdventOfCode2021
{
  public class Day02
  {
    public static void Solve(int part)
    {
      (int Offset, int Depth, int Aim) position = (0, 0, 0);
      using (StreamReader sr = new StreamReader("02.txt"))
      {
        string line;
        while ((line = sr.ReadLine()) != null)
        {
          string[] words = line.Split(' ');
          string command = words[0];
          int value = int.Parse(words[1]);
          if (part == 1)
          {
            switch (command)
            {
              case "forward":
                position.Offset += value;
                break;
              case "up":
                position.Depth -= value;
                break;
              case "down":
                position.Depth += value;
                break;
            }  
          }
          else
          {
            switch (command)
            {
              case "forward":
                position.Offset += value;
                position.Depth += position.Aim * value;
                break;
              case "up":
                position.Aim -= value;
                break;
              case "down":
                position.Aim += value;
                break;
            }
          }
        }
      }

      Console.WriteLine($"Sum of depth and offset is {position.Offset * position.Depth}"); 
    }
  }
}