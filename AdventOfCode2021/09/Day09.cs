﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AdventOfCode2021
{
  public class Day09
  {
    private class CaveLevel
    {
      public int Row { get; set; }
      public int Column { get; set; }
      public int Value { get; set; }
      public bool Marked { get; set; }

      public CaveLevel(int row, int column, int value)
      {
        Row = row;
        Column = column;
        Value = value;
        Marked = false;
      }
    }

    public static void Solve(int part)
    {
      CaveLevel[][] lines = File.ReadAllLines("09.txt").Select((x, i) => x.ToCharArray().Select((y, j) => new CaveLevel(i, j, y - '0')).ToArray()).ToArray();

      List<CaveLevel> riskLevels = new List<CaveLevel>();
      List<int> basins = new List<int>();
      
      for (int i = 0; i < lines.Length; i++)
      {
        for (int j = 0; j < lines[i].Length; j++)
        {
          if (lines[i][j].Value < GetNeighbors(lines, i, j).Min(x => x.Value))
          {
            riskLevels.Add(lines[i][j]);
          }
        }
      }
      
      if (part == 2)
      {
        foreach (CaveLevel riskLevel in riskLevels)
        {
          int basinSize = GetBasinSize(lines, riskLevel);
          basins.Add(basinSize);
        }
      }

      if (part == 1)
      {
        Console.WriteLine($"Sum of the risk levels is {riskLevels.Sum(x => x.Value + 1)}");
      }
      else
      {
        int[] relevantBasins = basins.OrderByDescending(x => x).Take(3).ToArray();
        Console.WriteLine($"Multiplication of the sizes of the three largest basins is {relevantBasins[0] * relevantBasins[1] * relevantBasins[2]}");
      }
    }
    
    private static List<CaveLevel> GetNeighbors(CaveLevel[][] lines, int i, int j)
    {
      List<CaveLevel> neighbors = new List<CaveLevel>();
      if (i > 0)
      {
        neighbors.Add(lines[i - 1][j]);
      }

      if (j > 0)
      {
        neighbors.Add(lines[i][j - 1]);
      }

      if (j < lines[i].Length - 1)
      {
        neighbors.Add(lines[i][j + 1]);
      }

      if (i < lines.Length - 1)
      {
        neighbors.Add(lines[i + 1][j]);
      }

      return neighbors;
    }

    private static int GetBasinSize(CaveLevel[][] lines, CaveLevel caveLevel)
    {
      if (caveLevel.Marked)
      {
        return 0;
      }
      int size = 1;
      lines[caveLevel.Row][caveLevel.Column].Marked = true;
      List<CaveLevel> neighbors = GetNeighbors(lines, caveLevel.Row, caveLevel.Column).Where(x => x.Value > caveLevel.Value && x.Value < 9 && !x.Marked).ToList();
      foreach (CaveLevel neighbor in neighbors)
      {
        size += GetBasinSize(lines, neighbor);
      }

      return size;
    }
  }
}