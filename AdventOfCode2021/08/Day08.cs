﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AdventOfCode2021
{
  public class Day08
  {
    private class CaveLevel
    {
      public int Row { get; set; }
      public int Column { get; set; }
      public int Value { get; set; }
      public bool Marked { get; set; }

      public CaveLevel(int row, int column, int value)
      {
        Row = row;
        Column = column;
        Value = value;
        Marked = false;
      }
    }
    
    public static void Solve(int part)
    {
      long result = 0;
      using StreamReader sr = new StreamReader("08.txt");
      string line;
      while ((line = sr.ReadLine()) != null)
      {
        string[] parts = line.Split("|");
        string[] inputs = parts[0].Split(" ").Where(x => x.Length > 0).ToArray();
        string[] outputs = parts[1].Split(" ").Where(x => x.Length > 0).ToArray();

        if (part == 1)
        {
          Dictionary<int, int[]> numbers = new Dictionary<int, int[]>
          {
            { 6, new [] { 0, 6, 9 } },
            { 2, new [] { 1 } },
            { 5, new [] { 2, 3, 5 } },
            { 4, new [] { 4 } },
            { 3, new [] { 7 } },
            { 7, new [] { 8 } }
          };
          
          foreach (string output in outputs)
          {
            if (numbers[output.Length].Length == 1)
            {
              result++;
            }
          }
        }
        else
        {
          Dictionary<int, char> decoder = new Dictionary<int, char>
          {
            { 0, '/' },
            { 1, '/' },
            { 2, '/' },
            { 3, '/' },
            { 4, '/' },
            { 5, '/' },
            { 6, '/' }
          };

          Dictionary<int, string> decodedNumbers = new Dictionary<int, string>
          {
            { 0, "" },
            { 1, "" },
            { 2, "" },
            { 3, "" },
            { 4, "" },
            { 5, "" },
            { 6, "" },
            { 7, "" },
            { 8, "" },
            { 9, "" },
          };

          Dictionary<int, List<int>> decodedOutput = new Dictionary<int, List<int>>
          {
            { 0, new List<int>{ 0, 1, 2, 4, 5, 6 } },
            { 1, new List<int>{ 2, 5 } },
            { 2, new List<int>{ 0, 2, 3, 4, 6 } },
            { 3, new List<int>{ 0, 2, 3, 5, 6 } },
            { 4, new List<int>{ 1, 2, 3, 5 } },
            { 5, new List<int>{ 0, 1, 3, 5, 6 } },
            { 6, new List<int>{ 0, 1, 3, 4, 5, 6 } },
            { 7, new List<int>{ 0, 2, 5 } },
            { 8, new List<int>{ 0, 1, 2, 3, 4, 5, 6 } },
            { 9, new List<int>{ 0, 1, 2, 3, 5, 6 } }
          };
          
          decodedNumbers[1] = inputs.First(x => x.Length == 2);
          decoder[2] = decodedNumbers[1][0];
          decoder[5] = decodedNumbers[1][1];

          decodedNumbers[4] = inputs.First(x => x.Length == 4);
          decodedNumbers[7] = inputs.First(x => x.Length == 3);
          decoder[0] = decodedNumbers[7].ToCharArray().First(x => !decodedNumbers[1].Contains(x));
        
          decodedNumbers[3] = inputs.First(x => x.Length == 5 && x.Contains(decoder[0]) && x.Contains(decoder[2]) && x.Contains(decoder[5]));
          decoder[3] = decodedNumbers[4].First(x => x != decoder[2] && x != decoder[5] && decodedNumbers[3].Contains(x));
          decoder[6] = decodedNumbers[3].First(x => x != decoder[2] && x != decoder[5] && x != decoder[0] && x != decoder[3]);
          decoder[1] = decodedNumbers[4].First(x => x != decoder[2] && x != decoder[5] && x != decoder[3]);

          decodedNumbers[8] = inputs.First(x => x.Length == 7);
          decoder[4] = decodedNumbers[8].First(x => !decoder.Values.Contains(x));

          decodedNumbers[2] = inputs.FirstOrDefault(x => x.Length == 5 && x.Contains(decoder[0]) && x.Contains(decoder[2]) && x.Contains(decoder[3]) && x.Contains(decoder[4]) && x.Contains(decoder[6]));
          if (decodedNumbers[2] == null)
          {
            (decoder[2], decoder[5]) = (decoder[5], decoder[2]);
          }

          string stringValue = "";
          foreach (string output in outputs)
          {
            List<int> nums = new List<int>();
            foreach (char c in output)
            {
              nums.Add(decoder.Keys.First(x => decoder[x] == c));
            }

            string joined = string.Join(",", nums.OrderBy(y => y).ToList());
            int decoded = decodedOutput.First(x => string.Join(",", x.Value).Equals(joined)).Key;
            stringValue += decoded;
          }

          result += int.Parse(stringValue);
        }
      }
      
      Console.WriteLine(part == 1 ? $"There are {result} of 1s, 4s, 7s and 8s." : $"The result is {result}");
    }
  }
}