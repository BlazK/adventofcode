﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AdventOfCode2021
{
  public class Day11
  {
    private static long Flashes = 0;
    
    private class Octopus
    {
      public int Row { get; set; }
      public int Column { get; set; }
      public int Energy { get; set; }
      public bool Flashed { get; set; }

      public Octopus(int row, int column, int energy)
      {
        Row = row;
        Column = column;
        Energy = energy;
        Flashed = false;
      }
    }
    
    public static void Solve(int part)
    {
      Octopus[][] octopuses = File.ReadAllLines("11/11.txt").Select((x, i) => x.Select((y, j) => new Octopus(i, j, y - '0')).ToArray()).ToArray();
      for (int step = 1; part == 1 ? step <= 100 : step < int.MaxValue; step++)
      {
        int flashedInStep = 0;
        foreach (Octopus[] row in octopuses)
        {
          foreach (Octopus octopus in row)
          {
            octopus.Energy++;
          }  
        }
        
        do
        {
          List<Octopus> octopusesToProcess = octopuses.SelectMany(x => x).Where(x => x.Energy > 9 && !x.Flashed).ToList();
          foreach (Octopus octopus in octopusesToProcess)
          {
            octopus.Flashed = true;
            Flashes++;
            flashedInStep++;
            foreach (Octopus neighbor in GetNeighbors(octopuses, octopus.Row, octopus.Column))
            {
              neighbor.Energy++;
            }
          }
        } while (octopuses.Any(x => x.Any(y => y.Energy > 9 && !y.Flashed)));
        
        foreach (Octopus[] row in octopuses)
        {
          foreach (Octopus octopus in row)
          {
            if (octopus.Flashed)
            {
              octopus.Flashed = false;
              octopus.Energy = 0;
            }
          }
        }

        if (flashedInStep == 100 && part == 2)
        {
          Console.WriteLine($"All flashed in step {step}");
          break; 
        }
      }

      if (part == 1)
      {
        Console.WriteLine($"There are {Flashes} flashes");  
      }
    }

    private static List<Octopus> GetNeighbors(Octopus[][] octopuses, int row, int column)
    {
      List<Octopus> neighbors = new List<Octopus>();
      if (row > 0)
      {
        if (column > 0)
        {
          neighbors.Add(octopuses[row - 1][column - 1]);
        }
        neighbors.Add(octopuses[row - 1][column]);
        if (column < octopuses[row].Length - 1)
        {
          neighbors.Add(octopuses[row - 1][column + 1]);
        }
      }

      if (column > 0)
      {
        neighbors.Add(octopuses[row][column - 1]);
      }

      if (column < octopuses[row].Length - 1)
      {
        neighbors.Add(octopuses[row][column + 1]);
      }

      if (row < octopuses.Length - 1)
      {
        if (column > 0)
        {
          neighbors.Add(octopuses[row + 1][column - 1]);
        }
        neighbors.Add(octopuses[row + 1][column]);
        if (column < octopuses[row].Length - 1)
        {
          neighbors.Add(octopuses[row + 1][column + 1]);
        }
      }

      return neighbors.Where(x => !x.Flashed).ToList();
    }
  }
}