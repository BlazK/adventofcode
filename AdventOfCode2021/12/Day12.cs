﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode2021
{
  public class Day12
  {
    private enum CaveSize
    {
      Small = 1,
      Big = 2
    }
    
    private class Cave
    {
      public string Name { get; set; }
      public CaveSize Size { get; set; }
      public bool IsStart { get; set; }
      public bool IsEnd { get; set; }
      public List<Cave> Neighbors { get; set; }

      public Cave()
      {
        
      }
      
      public Cave(string name)
      {
        Name = name;
        Size = new Regex("[A-Z]").IsMatch(Name.Substring(0, 1)) ? CaveSize.Big : CaveSize.Small;
        IsStart = Name.Equals("start");
        IsEnd = Name.Equals("end");
        Neighbors = new List<Cave>();
      }
    }

    private static List<string> Paths = new List<string>();
    
    public static void Solve(int part)
    {
      List<Cave> caves = new List<Cave>();
      using StreamReader sr = new StreamReader("12/12.txt");
      string line;
      while ((line = sr.ReadLine()) != null)
      {
        CreateCaves(caves, line.Split("-"));
      }

      Cave start = caves.First(x => x.IsStart);
      Process(start, start.Name, part);

      Console.WriteLine($"There are {Paths.Count} paths.");
    }

    private static void Process(Cave cave, string path, int part)
    {
      foreach (Cave neighbor in cave.Neighbors)
      {
        string newPath = $"{path},{neighbor.Name}";
        if (neighbor.IsEnd && !Paths.Contains(newPath))
        {
          Paths.Add(newPath);
        }
        else if (neighbor.Size == CaveSize.Big || neighbor.Size == CaveSize.Small && (part == 1 && path.Split(",").Count(x => x.Equals(neighbor.Name)) == 0 || part == 2 && Part2Criteria(newPath)))
        {
          Process(neighbor, newPath, part);
        }
      }
    }
    
    private static bool IsLowercase(string str) => new Regex("[a-z]").IsMatch(str.Substring(0, 1));

    private static bool Part2Criteria(string newPath)
    {
      List<string> words = newPath.Split(",").Where(IsLowercase).ToList();
      if (words.Count(x => x.Equals("start")) > 1)
      {
        return false;
      }
      
      string twiceWord = string.Empty;
      foreach (string word in words)
      {
        if (!string.IsNullOrEmpty(twiceWord) && twiceWord.Equals(word))
        {
          continue;
          
        }
        if (words.Count(x => x.Equals(word)) > 2)
        {
          return false;
        }
        
        if (words.Count(x => x.Equals(word)) == 2)
        {
          if (string.IsNullOrEmpty(twiceWord) && !word.Equals("start") && !word.Equals("end"))
          {
            twiceWord = word;
          }
          else
          {
            return false;
          }
        }
      }

      return true;
    }

    private static void CreateCaves(List<Cave> caves, string[] words)
    {
      Cave cave1 = caves.FirstOrDefault(x => x.Name.Equals(words[0]));
      if (cave1 == null)
      {
        cave1 = new Cave(words[0]);
        caves.Add(cave1); 
      }
      
      Cave cave2 = caves.FirstOrDefault(x => x.Name.Equals(words[1]));
      if (cave2 == null)
      {
        cave2 = new Cave(words[1]);
        caves.Add(cave2);
      }

      if (!cave1.Neighbors.Any(x => x.Name.Equals(cave2.Name)))
      {
        cave1.Neighbors.Add(cave2);
      }
      
      if (!cave2.Neighbors.Any(x => x.Name.Equals(cave1.Name)))
      {
        cave2.Neighbors.Add(cave1);
      }
    }
  }
}