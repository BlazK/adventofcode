﻿using AdventOfCode2020.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.Design;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Runtime;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;

namespace AdventOfCode2020
{
  class Program
  {
    static void Main(string[] args)
    {
      //Day01();
      //Day02();
      //Day03();
      //Day04();
      //Day05();
      //Day06();
      //Day07();
      //Day08();
      //Day09();
      //Day10();
      //Day11();
      //Day12();
      //Day13();
      //Day14();
      //Day15();
      //Day16();
      Day17();
    }

    #region Day 1

    private static void Day01()
    {
      Day01Part01();
      Day01Part02();
    }

    private static void Day01Part01()
    {
      List<int> numbers = new List<int>();
      using (StreamReader sr = new StreamReader("Inputs/1.txt"))
      {
        string line;
        while ((line = sr.ReadLine()) != null)
        {
          numbers.Add(int.Parse(line));
        }
      }

      int num1 = -1;
      int num2 = -1;
      foreach (int number in numbers)
      {
        foreach (int number2 in numbers)
        {
          if (number != number2 && number + number2 == 2020)
          {
            num1 = number;
            num2 = number2;
            goto END;
          }
        }
      }

      END: Console.WriteLine($"Numbers are {num1} and {num2}.\nMulitplication = {num1 * num2}");
    }

    private static void Day01Part02()
    {
      List<int> numbers = new List<int>();
      using (StreamReader sr = new StreamReader("Inputs/1.txt"))
      {
        string line;
        while ((line = sr.ReadLine()) != null)
        {
          numbers.Add(int.Parse(line));
        }
      }

      int num1 = -1;
      int num2 = -1;
      int num3 = -1;
      for (int i = 0; i < numbers.Count; i++)
      {
        for (int j = 0; j < numbers.Count; j++)
        {
          for (int k = 0; k < numbers.Count; k++)
          {
            if (i != j && j != k && i != k && numbers[i] + numbers[j] + numbers[k] == 2020)
            {
              num1 = numbers[i];
              num2 = numbers[j];
              num3 = numbers[k];
            }
          }
        }
      }

      END: Console.WriteLine($"Numbers are {num1}, {num2} and {num3}.\nMulitplication = {num1 * num2 * num3}");
    }

    #endregion

    #region Day 2

    private static void Day02()
    {
      Day02Part01();
      Day02Part02();
    }

    private static void Day02Part01()
    {
      int valid = 0;
      using (StreamReader sr = new StreamReader("Inputs/2.txt"))
      {
        string line;
        while ((line = sr.ReadLine()) != null)
        {
          string[] words = line.Split(' ');
          int[] fromTo = words[0].Split('-').Select(int.Parse).ToArray();
          char c = words[1].ToCharArray(0, 1).First();
          char[] password = words[2].ToCharArray();
          int total = 0;
          foreach (char letter in password)
          {
            if (letter == c)
            {
              total++;
            }
          }

          if (total >= fromTo[0] && total <= fromTo[1])
          {
            valid++;
          }
        }
      }

      Console.WriteLine($"Total number of valid passwords: {valid}.");
    }

    private static void Day02Part02()
    {
      int valid = 0;
      using (StreamReader sr = new StreamReader("Inputs/2.txt"))
      {
        string line;
        while ((line = sr.ReadLine()) != null)
        {
          string[] words = line.Split(' ');
          int[] indices = words[0].Split('-').Select(x => int.Parse(x) - 1).ToArray();
          char c = words[1].ToCharArray(0, 1).First();
          char[] password = words[2].ToCharArray();
          if (password[indices[0]] == c && password[indices[1]] != c || password[indices[0]] != c && password[indices[1]] == c)
          {
            valid++;
          }
        }
      }

      Console.WriteLine($"Total number of valid passwords: {valid}.");
    }

    #endregion

    #region Day 3

    private static void Day03()
    {
      Day03Part01(3, 1, true);
      Day03Part02();
    }

    private static int Day03Part01(int right, int down, bool printOutput)
    {
      int x = 0;
      int y = 0;
      int skipper = 0;
      int trees = 0;
      using (StreamReader sr = new StreamReader("Inputs/3.txt"))
      {
        string line;
        while ((line = sr.ReadLine()) != null)
        {
          skipper--;
          if (skipper > 0)
          {
            continue;
          }

          char[] chars = line.ToCharArray();
          if (chars[x] == '#')
          {
            trees++;
          }

          x = (x + right) % line.Length;
          skipper = down;
        }
      }

      if (printOutput)
      {
        Console.WriteLine($"Total number of trees: {trees}.");
      }

      return trees;
    }

    private static void Day03Part02()
    {
      int[][] slopes = { new[] { 1, 1 }, new[] { 3, 1 }, new[] { 5, 1 }, new[] { 7, 1 }, new[] { 1, 2 } };
      int total = 1;
      for (int i = 0; i < slopes.Length; i++)
      {
        total *= Day03Part01(slopes[i][0], slopes[i][1], false);
      }

      Console.WriteLine($"Total number of trees: {total}.");
    }

    #endregion

    #region Day 4

    private static void Day04()
    {
      Day04Part01(false);
      Day04Part02();
    }

    private static void Day04Part01(bool validateAdvanced)
    {
      int valid = 0;
      using (StreamReader sr = new StreamReader("Inputs/4.txt"))
      {
        Passport passport = new Passport();
        string line;
        bool ignoreAddingDueToPossibleCountryId = false;
        while ((line = sr.ReadLine()) != null)
        {
          if (line.Length == 0)
          {
            passport = new Passport();
            ignoreAddingDueToPossibleCountryId = false;
            continue;
          }

          string[][] values = line.Split(' ').Select(x => x.Split(':').ToArray()).ToArray();
          foreach (string[] keyValuePair in values)
          {
            switch (keyValuePair[0])
            {
              case "byr":
                passport.BirthYear = keyValuePair[1];
                break;
              case "iyr":
                passport.IssueYear = keyValuePair[1];
                break;
              case "eyr":
                passport.ExpirationYear = keyValuePair[1];
                break;
              case "hgt":
                passport.Height = keyValuePair[1];
                break;
              case "hcl":
                passport.HairColor = keyValuePair[1];
                break;
              case "ecl":
                passport.EyeColor = keyValuePair[1];
                break;
              case "pid":
                passport.PassportId = keyValuePair[1];
                break;
            }
          }

          if (!ignoreAddingDueToPossibleCountryId && IsValid(passport))
          {
            valid++;
            ignoreAddingDueToPossibleCountryId = true;
          }
        }
      }

      Console.WriteLine($"Total number of valid passwords: {valid}.");

      bool IsValid(Passport passport)
      {
        if (string.IsNullOrEmpty(passport.BirthYear) ||
           string.IsNullOrEmpty(passport.IssueYear) ||
           string.IsNullOrEmpty(passport.ExpirationYear) ||
           string.IsNullOrEmpty(passport.Height) ||
           string.IsNullOrEmpty(passport.HairColor) ||
           string.IsNullOrEmpty(passport.EyeColor) ||
           string.IsNullOrEmpty(passport.PassportId))
        {
          return false;
        }

        if (!validateAdvanced)
        {
          return true;
        }

        return new Regex("^19[2-9][0-9]$|^200[0-2]$").IsMatch(passport.BirthYear) &&
               new Regex("^201[0-9]$|^2020$").IsMatch(passport.IssueYear) &&
               new Regex("^202[0-9]$|^2030$").IsMatch(passport.ExpirationYear) &&
               new Regex("^1[5-8][0-9]cm$|^19[0-3]cm$|^59in$|^6[0-9]in$|^7[0-6]in$").IsMatch(passport.Height) &&
               new Regex("^#[0-9a-f]{6}$").IsMatch(passport.HairColor) &&
               new Regex("^amb$|^blu$|^brn$|^gry$|^grn$|^hzl$|^oth$").IsMatch(passport.EyeColor) &&
               new Regex("^[0-9]{9}$").IsMatch(passport.PassportId);
      }
    }

    private static void Day04Part02()
    {
      Day04Part01(true);
    }

    #endregion

    #region Day 5

    private static void Day05()
    {
      Day05Part01();
      Day05Part02();
    }

    private static void Day05Part01()
    {
      int max = 0;
      using (StreamReader sr = new StreamReader("Inputs/5.txt"))
      {
        string line;
        while ((line = sr.ReadLine()) != null)
        {
          int from = 0;
          int to = 127;
          char[] chars = line.ToCharArray();
          int rowIndex = -1;
          foreach (char c in chars)
          {
            int diff = to - from + 1;
            switch (c)
            {
              case 'F':
              case 'L':
                to -= diff / 2;
                break;
              case 'R':
              case 'B':
                from += diff / 2;
                break;
            }

            if (from == to && rowIndex == -1)
            {
              rowIndex = from;
              from = 0;
              to = 7;
            }
          }

          int id = rowIndex * 8 + from;
          if(id > max)
          {
            max = id;
          }
        }
      }

      Console.WriteLine($"Highest seat ID is: {max}.");
    }

    private static void Day05Part02()
    {
      List<int> seatIds = new List<int>();
      using (StreamReader sr = new StreamReader("Inputs/5.txt"))
      {
        string line;
        while ((line = sr.ReadLine()) != null)
        {
          int from = 0;
          int to = 127;
          char[] chars = line.ToCharArray();
          int rowIndex = -1;
          foreach (char c in chars)
          {
            int diff = to - from + 1;
            switch (c)
            {
              case 'F':
              case 'L':
                to -= diff / 2;
                break;
              case 'R':
              case 'B':
                from += diff / 2;
                break;
            }

            if (from == to && rowIndex == -1)
            {
              rowIndex = from;
              from = 0;
              to = 7;
            }
          }

          seatIds.Add(rowIndex * 8 + from);
        }
      }

      seatIds = seatIds.OrderBy(x => x).ToList();
      int seatId = -1;
      for (int i = 0; i < seatIds.Count - 1; i++)
      {
        if (seatIds[i] == seatIds[i + 1] - 2)
        {
          seatId = seatIds[i] + 1;
          break;
        }
      }

      Console.WriteLine($"Your seat ID is: {seatId}.");
    }

    #endregion

    #region Day 6

    private static void Day06()
    {
      Day06Part01();
      Day06Part02();
    }

    private static void Day06Part01()
    {
      int sum = 0;
      using (StreamReader sr = new StreamReader("Inputs/6.txt"))
      {
        HashSet<char> group = new HashSet<char>();
        string line;
        while ((line = sr.ReadLine()) != null)
        {
          if (line.Length == 0)
          {
            sum += group.Count;
            group = new HashSet<char>();
          }

          foreach (char c in line)
          {
            if(!group.Contains(c))
            {
                group.Add(c);
            }
          }
        }

        sum += group.Count;
      }

      Console.WriteLine($"Sum is {sum}.");
    }

    private static void Day06Part02()
    {
      int sum = 0;
      using (StreamReader sr = new StreamReader("Inputs/6.txt"))
      {
        Dictionary<char, int> group = new Dictionary<char, int>();
        string line;
        int numberOfPeopleInGroup = 0;
        while ((line = sr.ReadLine()) != null)
        {
          if (line.Length == 0)
          {
            sum += group.Count(x => x.Value == numberOfPeopleInGroup);
            group = new Dictionary<char, int>();
            numberOfPeopleInGroup = 0;
          }
          else
          {
            numberOfPeopleInGroup++;
          }

          foreach (char c in line)
          {
            if (!group.ContainsKey(c))
            {
              group.Add(c, 1);
            }
            else
            {
              group[c]++;
            }
          }
        }

        sum += group.Count(x => x.Value == numberOfPeopleInGroup);
      }

      Console.WriteLine($"Sum is {sum}.");
    }

    #endregion

    #region Day 7

    private static void Day07()
    {
      Day07Part01();
      Day07Part02();
    }

    private static List<Bag> GetBags()
    {
      List<Bag> bags = new List<Bag>();
      using (StreamReader sr = new StreamReader("Inputs/7.txt"))
      {
        string line;
        while ((line = sr.ReadLine()) != null)
        {
          string[] words = line.Split(' ');
          string key = $"{words[0]}_{words[1]}";
          Bag bag = bags.FirstOrDefault(x => x.Key.Equals(key));
          if (bag == null)
          {
            bag = new Bag(key);
          }

          if (words.Length > 7)
          {
            for (int i = 4; i < words.Length - 2; i += 4)
            {
              int n = int.Parse(words[i]);
              string childKey = $"{words[i + 1]}_{words[i + 2]}";
              Bag child = bags.FirstOrDefault(x => x.Key.Equals(childKey));
              if (child == null)
              {
                child = new Bag(childKey);
                bags.Add(child);
              }

              child.Parents.Add(bag);
              bag.Children.Add((child, n));
            }
          }

          if (!bags.Any(x => x.Key.Equals(key)))
          {
            bags.Add(bag);
          }
        }
      }

      return bags;
    }

    private static void Day07Part01()
    {
      List<Bag> bags = GetBags();
      Bag shinyGold = bags.First(x => x.Key.Equals("shiny_gold"));
      HashSet<string> bagKeys = new HashSet<string>();
      FillParentsSet(shinyGold, bagKeys);
      Console.WriteLine($"There are {bagKeys.Count} possible bag colors.");

      static void FillParentsSet(Bag bag, HashSet<string> bagKeys)
      {
        foreach (Bag parent in bag.Parents)
        {
          bagKeys.Add(parent.Key);
          FillParentsSet(parent, bagKeys);
        }
      }
    }

    private static void Day07Part02()
    {
      List<Bag> bags = GetBags();
      Bag shinyGold = bags.First(x => x.Key.Equals("shiny_gold"));
      Console.WriteLine($"There are {GetChildrenCount(shinyGold) - 1} possible bag colors.");

      static int GetChildrenCount(Bag bag)
      {
        if (bag.Children.Count == 0)
        {
          return 1;
        }

        int sum = 1;
        foreach ((Bag child, int n) in bag.Children)
        {
          sum += n * GetChildrenCount(child);
        }

        return sum;
      }
    }

    #endregion

    #region Day 8

    private static void Day08()
    {
      Day08Part01();
      Day08Part02();
    }

    private static (bool IsEndlessLoop, int Accumulator) Day08Part01(int? changeJmpOrNopIndex = null, bool printOutput = true)
    {
      int accumulator = 0;
      bool alreadyChangedInstruction = false;
      int instructionVisitedIndex = 0;
      bool isEndlessLoop = false;
      using (StreamReader sr = new StreamReader("Inputs/8.txt"))
      {
        HashSet<int> instructions = new HashSet<int>();
        string[] lines = sr.ReadToEnd().Split('\n');
        for(int lineNumber = 0; lineNumber < lines.Length; lineNumber++) {
          string[] words = lines[lineNumber].Split(' ');
          string instruction = words[0];
          int n = int.Parse(words[1]);

          if (instructions.Contains(lineNumber))
          {
            isEndlessLoop = true;
            break;
          }
          
          instructions.Add(lineNumber);

          switch (instruction)
          {
            case "acc":
              accumulator += n;
              break;
            case "jmp":
              if (changeJmpOrNopIndex.HasValue && !alreadyChangedInstruction && changeJmpOrNopIndex.Value == instructionVisitedIndex)
              {
                alreadyChangedInstruction = true;
                continue;
              }

              instructionVisitedIndex++;
              lineNumber += n - 1;
              break;
            case "nop":
              if (changeJmpOrNopIndex.HasValue && changeJmpOrNopIndex.Value == instructionVisitedIndex && !alreadyChangedInstruction)
              {
                alreadyChangedInstruction = true;
                lineNumber += n - 1;
              }

              instructionVisitedIndex++;
              continue;
          }
        }
      }

      if (printOutput)
      {
        Console.WriteLine($"Accumulator's value is: {accumulator}.");
      }
      
      return (isEndlessLoop, accumulator);
    }

    private static void Day08Part02()
    {
      int jmpOrNopIndex = 0;
      while (true)
      {
        (bool isEndlessLoop, int accumulator) = Day08Part01(jmpOrNopIndex, false);
        if (!isEndlessLoop)
        {
          Console.WriteLine($"The final accumulator's value is: {accumulator}");
          break;
        }
        jmpOrNopIndex++;
      }
    }

    #endregion

    #region Day 9

    private static void Day09()
    {
      Day09Part01();
      Day09Part02();
    }

    private static (int failingNumber, List<int> numbers) Day09Part01(bool printOutput = true)
    {
      int failingNumber = 0;
      List<int> numbers = new List<int>();
      using (StreamReader sr = new StreamReader("Inputs/9.txt"))
      {
        string line;
        int preambleCount = 0;
        while ((line = sr.ReadLine()) != null)
        {
          int number = int.Parse(line);
          if (preambleCount <= 25)
          {
            numbers.Add(number);
            preambleCount++;
          }
          else
          {
            List<int> last25numbers = numbers.GetRange(numbers.Count - 25, 25);
            bool exists = last25numbers.Exists(x => last25numbers.Exists(y => last25numbers.IndexOf(x) != last25numbers.IndexOf(y) && x + y == number));
            if (!exists)
            {
              failingNumber = number;
              break;
            }

            numbers.Add(number);
          }
        }
      }

      if (printOutput)
      {
        Console.WriteLine($"Failing number is: {failingNumber}.");
      }

      return (failingNumber, numbers);
    }

    private static void Day09Part02()
    {
      (int failingNumber, List<int> numbers) = Day09Part01(false);
      int startIndex = 0;
      
      (int min, int max)? minMax = null;
      while (true)
      {
        int sum = 0;
        for (int i = startIndex; i < numbers.Count; i++)
        {
          sum += numbers[i];
          if (sum == failingNumber)
          {
            List<int> range = numbers.GetRange(startIndex, i - startIndex + 1);
            minMax = (range.Min(), range.Max());
            break;
          }
          
          if (sum > failingNumber)
          {
            break;
          }
        }

        if(minMax.HasValue)
        {
          break;
        }

        startIndex++;
      }
      Console.WriteLine($"Min max sum is: {minMax?.min + minMax?.max}.");
    }

    #endregion

    #region Day 10

    private static void Day10()
    {
      Day10Part01(GetNumbers());
      Day10Part02(GetNumbers());
    }

    private static List<int> GetNumbers()
    {
      using (StreamReader sr = new StreamReader("Inputs/10.txt"))
      {
        List<int> numbers = sr.ReadToEnd().Split('\n').Select(int.Parse).OrderBy(x => x).ToList();
        numbers.Insert(0, 0);
        numbers.Insert(numbers.Count, numbers[^1] + 3);
        return numbers;
      }
    }

    private static JoltDiffs Day10Part01(List<int> numbers, bool printOutput = true)
    {
      JoltDiffs joltDiffs = new JoltDiffs();
      int previous = 0;
      int n = numbers.Count;
      for (int i = 0; i < n; i++)
      {
        int number = numbers[0];
        int diff = number - previous;
        switch (diff)
        {
          case 1:
            joltDiffs.One.Add(i);
            break;
          case 3:
            joltDiffs.Three.Add(i);
            break;
        }

        previous = number;
        numbers.RemoveAt(0);
      }

      if (printOutput)
      {
        Console.WriteLine($"Jolt 1 difference multiplied by jolt 3 difference is: {joltDiffs.One.Count * joltDiffs.Three.Count}.");
      }

      return joltDiffs;
    }

    private static Dictionary<long, long> Cache = new Dictionary<long, long>();
    private static void Day10Part02(List<int> numbers)
    {
      long arrangementsCount = DynamicProgramming(0, numbers);
      Console.WriteLine($"Total number of possible arrangements is {arrangementsCount}");
    }

    private static long DynamicProgramming(int i, List<int> numbers)
    {
      if (i == numbers.Count - 1)
      {
        return 1;
      }

      if (Cache.ContainsKey(i))
      {
        return Cache[i];
      }

      long total = 0;
      for (int j = i + 1; j < numbers.Count; j++)
      {
        if (numbers[j] - numbers[i] <= 3)
        {
          total += DynamicProgramming(j, numbers);
        }
      }
      Cache.Add(i, total);

      return total;
    }

    #endregion

    #region Day 11

    private static void Day11()
    {
      Day11Part(1);
      Day11Part(2);
    }

    private static char[,] GetDay11Seats()
    {
      using (StreamReader sr = new StreamReader("Inputs/11.txt"))
      {
        string[] lines = sr.ReadToEnd().Split('\n');
        char[,] seats = new char[lines.Length, lines[0].Length];
        for (int i = 0; i < lines.Length; i++)
        {
          for (int j = 0; j < lines[i].Length; j++)
          {
            seats[i, j] = lines[i][j];
          }
        }

        return seats;
      }
    }

    private static void Day11Part(int part)
    {
      char[,] seats = GetDay11Seats();
      string previousState = string.Join('\n', seats);
      int rows = seats.GetLength(0);
      int columns = seats.GetLength(1);
      while (true)
      {
        StringBuilder currentState = new StringBuilder();
        char[,] seatsCopy = (char[,])seats.Clone();
        for (int i = 0; i < rows; i++)
        {
          StringBuilder sb = new StringBuilder();
          for (int j = 0; j < columns; j++)
          {
            if (seatsCopy[i, j] != '.')
            {
              (int free, int occupied) neighbors = GetSeatNeighbors(seatsCopy, i, j, part);
              if (seatsCopy[i, j] == 'L' && neighbors.occupied == 0)
              {
                seats[i, j] = '#';
              }
              else if (seatsCopy[i, j] == '#' && (part == 1 ? neighbors.occupied >= 4 : neighbors.occupied >= 5))
              {
                seats[i, j] = 'L';
              }

              sb.Append(seats[i, j]);
            }
            else
            {
              sb.Append(seats[i, j]);
            }
          }

          currentState.Append($"{sb}\n");
        }

        string currentStateString = currentState.ToString();
        if (currentStateString == previousState)
        {
          break;
        }

        previousState = currentStateString;
      }

      Console.WriteLine($"There are {previousState.Count(x => x == '#')} occupied seats.");
    }

    private static (int free, int occupied) GetSeatNeighbors(char[,] seats, int row, int column, int part)
    {
      int rowCount = seats.GetLength(0);
      int rowLength = seats.GetLength(1);
      (int free, int occupied) neighbors = (0, 0);
      // Top
      for (int i = row - 1; part == 1 ? i >= row - 1 && i >= 0: i >= 0; i--)
      {
        if(seats[i, column] == 'L')
        {
          neighbors.free++;
          break;
        }

        if (seats[i, column] == '#')
        {
          neighbors.occupied++;
          break;
        }
      }
      // Bottom
      for (int i = row + 1; part == 1 ? i <= row + 1 && i < rowCount: i < rowCount; i++)
      {
        if (seats[i, column] == 'L')
        {
          neighbors.free++;
          break;
        }

        if (seats[i, column] == '#')
        {
          neighbors.occupied++;
          break;
        }
      }
      // Left
      for (int i = column - 1; part == 1 ? i >= column - 1 && i >= 0: i >= 0; i--)
      {
        if (seats[row, i] == 'L')
        {
          neighbors.free++;
          break;
        }

        if (seats[row, i] == '#')
        {
          neighbors.occupied++;
          break;
        }
      }
      // Right
      for (int i = column + 1; part == 1 ? i <= column + 1 && i < rowLength : i < rowLength; i++)
      {
        if (seats[row, i] == 'L')
        {
          neighbors.free++;
          break;
        }

        if (seats[row, i] == '#')
        {
          neighbors.occupied++;
          break;
        }
      }
      // Top-left
      for (int i = row - 1, j = column - 1; part == 1 ? i >= row - 1 && j >= column - 1 && i >= 0 && j >= 0 : i >= 0 && j >= 0; i--, j--)
      {
        if (seats[i, j] == 'L')
        {
          neighbors.free++;
          break;
        }

        if (seats[i, j] == '#')
        {
          neighbors.occupied++;
          break;
        }
      }
      // Top-right
      for (int i = row - 1, j = column + 1; part == 1 ? i >= row - 1 && j <= column + 1 && i >= 0 && j < rowLength: i >= 0 && j < rowLength; i--, j++)
      {
        if (seats[i, j] == 'L')
        {
          neighbors.free++;
          break;
        }

        if (seats[i, j] == '#')
        {
          neighbors.occupied++;
          break;
        }
      }
      // Bottom-left
      for (int i = row + 1, j = column - 1; part == 1 ? i <= row + 1 && j >= column - 1 && i < rowCount && j >= 0 : i < rowCount && j >= 0; i++, j--)
      {
        if (seats[i, j] == 'L')
        {
          neighbors.free++;
          break;
        }

        if (seats[i, j] == '#')
        {
          neighbors.occupied++;
          break;
        }
      }
      // Bottom-right
      for (int i = row + 1, j = column + 1; part == 1 ? i <= row + 1 && j <= column + 1 && i < rowCount && j < rowLength: i < rowCount && j < rowLength; i++, j++)
      {
        if (seats[i, j] == 'L')
        {
          neighbors.free++;
          break;
        }

        if (seats[i, j] == '#')
        {
          neighbors.occupied++;
          break;
        }
      }

      return neighbors;
    }

    #endregion

    #region Day 12

    private static void Day12()
    {
      Day12Part01();
      Day12Part02();
    }

    private static void Day12Part01()
    {
      (int x, int y) position = (0, 0);
      // CSS-based orientation (north = 0, east = 1, south = 2, west = 3)
      int angle = 0;
      using (StreamReader sr = new StreamReader("Inputs/12.txt"))
      {
        string line;
        while ((line = sr.ReadLine()) != null)
        {
          char instruction = line[0];
          int value = int.Parse(line.Substring(1));
          switch (instruction)
          {
            case 'N':
              position.y += value;
              break;
            case 'S':
              position.y -= value;
              break;
            case 'E':
              position.x += value;
              break;
            case 'W':
              position.x -= value;
              break;
            case 'F':
              switch (angle)
              {
                case 0:
                  position.x += value;
                  break;
                case 90:
                  position.y += value;
                  break;
                case 180:
                  position.x -= value;
                  break;
                case 270:
                  position.y -= value;
                  break;
              }
              break;
            case 'R':
              int finalRight = angle - value;
              angle = finalRight < 0 ? finalRight + 360 : finalRight;
              break;
            case 'L':
              int finalLeft = angle + value;
              angle = finalLeft >= 360 ? finalLeft - 360 : finalLeft;
              break;
          }
        }
      }

      Console.WriteLine($"Final position is: {Math.Abs(position.x) + Math.Abs(position.y)}.");
    }

    private static void Day12Part02()
    {
      (int x, int y) position = (0, 0);
      (int x, int y) waypoint = (10, 1);
      // CSS-based orientation (north = 0, east = 1, south = 2, west = 3)
      int angle = 0;
      using (StreamReader sr = new StreamReader("Inputs/12.txt"))
      {
        string line;
        while ((line = sr.ReadLine()) != null)
        {
          char instruction = line[0];
          int value = int.Parse(line.Substring(1));
          switch (instruction)
          {
            case 'N':
              waypoint.y += value;
              break;
            case 'S':
              waypoint.y -= value;
              break;
            case 'E':
              waypoint.x += value;
              break;
            case 'W':
              waypoint.x -= value;
              break;
            case 'F':
              position.x += waypoint.x * value;
              position.y += waypoint.y * value;
              break;
            case 'R':
              int finalRight = angle - value;
              int newAngleRight = finalRight < 0 ? finalRight + 360 : finalRight;
              for (int i = 0; i < value / 90; i++)
              {
                waypoint = (waypoint.y, -waypoint.x);
              }

              angle = newAngleRight;
              break;
            case 'L':
              int finalLeft = angle + value;
              int newAngleLeft = finalLeft >= 360 ? finalLeft - 360 : finalLeft;
              for (int i = 0; i < value / 90; i++)
              {
                waypoint = (-waypoint.y, waypoint.x);
              }
              
              angle = newAngleLeft;
              break;
          }
        }
      }

      Console.WriteLine($"Final position is: {Math.Abs(position.x) + Math.Abs(position.y)}.");
    }

    #endregion

    #region Day 13

    private static void Day13()
    {
      Day13Part01();
      Day13Part02();
    }

    private static void Day13Part01()
    {
      using (StreamReader sr = new StreamReader("Inputs/13.txt"))
      {
        int timestamp = int.Parse(sr.ReadLine());
        int index = 0;
        int minDiff = int.MaxValue;
        (int busNumber, int departs)[] buses = sr.ReadLine()
          .Split(',')
          .Where(x => int.TryParse(x, out _))
          .Select(x => (int.Parse(x), 0))
          .ToArray();
        for(int i = 0; i < buses.Length; i++)
        {
          while (buses[i].departs < timestamp)
          {
            buses[i].departs += buses[i].busNumber;
          }

          int diff = buses[i].departs - timestamp;
          if (diff < minDiff)
          {
            minDiff = diff;
            index = i;
          }
        }

        Console.WriteLine($"Bus ID multiplied by the number of minutes to wait: {buses[index].busNumber * (buses[index].departs - timestamp)}");
      }
    }

    private static void Day13Part02()
    {
      using (StreamReader sr = new StreamReader("Inputs/13.txt"))
      {
        string _empty = sr.ReadLine();
        List<ulong> buses = sr.ReadLine()
          .Split(',')
          .Select(x => ulong.TryParse(x, out ulong _) ? ulong.Parse(x) : 0)
          .ToList();

        ulong startTimestamp = buses[0];
        ulong inc = buses[0];
        int index = 1;
        while(index < buses.Count)
        {
          if (buses[index] == 0)
          {
            index++;
            continue;
          }
          while (true)
          {
            if ((startTimestamp + (ulong)index) % buses[index] == 0)
            {
              inc = Lcm(inc, buses[index]);
              break;
            }

            startTimestamp += inc;
          }

          index++;
        }

        Console.WriteLine($"Earliest timestamp for listed offsets is: {startTimestamp}");
      }
    }

    #endregion

    #region Day 14

    private static void Day14()
    {
      Day14Part01();
      Day14Part02();
    }

    private static string NumberToBinaryString(ulong number, int totalLength)
    {
      string result = "";
      while (number > 1)
      {
        int remainder = (int)number % 2;
        result = Convert.ToString(remainder) + result;
        number /= 2;
      }

      string prefix = "";
      result = Convert.ToString(number) + result;
      for (int i = result.Length; i < totalLength; i++)
      {
        prefix += "0";
      }

      return prefix + result;
    }

    private static ulong BinaryStringToNumber(string str)
    {
      double sum = 0;
      for (int i = 35; i >= 0; i--)
      {
        if (str[i] == '1')
        {
          sum += Math.Pow(2, 35 - i);
        }
      }

      return (ulong) sum;
    }

    private static void Day14Part01()
    {
      Dictionary<int, ulong> memory = new Dictionary<int, ulong>();
      using (StreamReader sr = new StreamReader("Inputs/14.txt"))
      {
        string line;
        string mask = "";
        while ((line = sr.ReadLine()) != null)
        {
          if (line.StartsWith("mask"))
          {
            mask = line.Substring(7);
          }
          else
          {
            int startIndex = line.IndexOf('[');
            int endIndex = line.IndexOf(']');
            int memorySpace = int.Parse(line.Substring(startIndex + 1, endIndex - startIndex - 1));
            ulong number = ulong.Parse(line.Substring(line.IndexOf('=') + 2));

            string numberString = NumberToBinaryString(number, mask.Length);
            double finalValue = 0;
            for(int i = 35; i >= 0; i--)
            {
              if (mask[i] == '1' || mask[i] == 'X' && numberString[i] == '1')
              {
                finalValue += Math.Pow(2, 35 - i);
              }
            }

            AddOrUpdateDictionary(memory, memorySpace, (uint)finalValue);
          }
        }
      }

      Console.WriteLine($"Final sum after initialization is {memory.Where(x => x.Value > 0).Sum(x => (decimal)x.Value)}");
    }

    private static void Day14Part02()
    {
      Dictionary<ulong, ulong> memory = new Dictionary<ulong, ulong>();
      using (StreamReader sr = new StreamReader("Inputs/14.txt"))
      {
        string line;
        string mask = "";
        while ((line = sr.ReadLine()) != null)
        {
          if (line.StartsWith("mask"))
          {
            mask = line.Substring(7);
          }
          else
          {
            int startIndex = line.IndexOf('[');
            int endIndex = line.IndexOf(']');
            int address = int.Parse(line.Substring(startIndex + 1, endIndex - startIndex - 1));
            char[] addressChars = NumberToBinaryString((ulong)address, mask.Length).ToCharArray();
            ulong number = ulong.Parse(line.Substring(line.IndexOf('=') + 2));

            for (int i = mask.Length - 1; i >= 0; i--)
            {
              if (mask[i] != '0')
              {
                addressChars[i] = mask[i];
              }
            }
            string addressString = new string(addressChars);

            int numberOfXs = addressString.Count(x => x == 'X');
            int n = (int) Math.Pow(2, numberOfXs);
            for (int i = 0; i < n; i++)
            {
              string str = NumberToBinaryString((ulong)i, numberOfXs);
              char[] finalStr = addressString.ToCharArray();
              for (int j = 0, index = 0; j < finalStr.Length; j++)
              {
                if (finalStr[j] == 'X')
                {
                  finalStr[j] = str[index++];
                }
              }

              string finalString = new string(finalStr);
              AddOrUpdateDictionary(memory, BinaryStringToNumber(finalString), number);
            }
          }
        }
      }

      Console.WriteLine($"Final sum after initialization is {memory.Where(x => x.Value > 0).Sum(x => (decimal)x.Value)}");
    }

    #endregion

    #region Day 15

    private static void Day15()
    {
      int[] numbers = {2, 0, 1, 9, 5, 19};
      Day15Part(numbers, 2020);
      Day15Part(numbers, 30000000);
    }

    private static void Day15Part(int[] numbers, int stopTurn)
    {
      Dictionary<int, List<int>> spokenNumbers = new Dictionary<int, List<int>>(); // <number, List<turnThatWasSpoken>>
      int lastSpokenNumber = -1;

      for (int i = 0; i < numbers.Length; i++)
      {
        AddOrUpdateDictionary(spokenNumbers, numbers[i], new List<int>{i});
        lastSpokenNumber = numbers[i];
      }

      int turn = numbers.Length;
      while (turn < stopTurn)
      {
        if (spokenNumbers.ContainsKey(lastSpokenNumber))
        {
          if (spokenNumbers[lastSpokenNumber].Count == 1)
          {
            lastSpokenNumber = 0;
            if (!spokenNumbers.ContainsKey(lastSpokenNumber))
            {
              spokenNumbers.Add(lastSpokenNumber, new List<int> { turn });
            }
            else
            {
              spokenNumbers[lastSpokenNumber].Add(turn);
            }
          }
          else
          {
            lastSpokenNumber = spokenNumbers[lastSpokenNumber][^1] - spokenNumbers[lastSpokenNumber][^2];
            if (!spokenNumbers.ContainsKey(lastSpokenNumber))
            {
              spokenNumbers.Add(lastSpokenNumber, new List<int>{turn});
            }
            else
            {
              spokenNumbers[lastSpokenNumber].Add(turn);
            }
          }
        }
        else
        {
          spokenNumbers.Add(lastSpokenNumber, new List<int>{turn});
          lastSpokenNumber = 0;
        }

        turn++;
      }

      Console.WriteLine($"2020th number spoken is: {lastSpokenNumber}");
    }

    #endregion

    #region Day 16

    private static void Day16()
    {
      Day16Part01();
      Day16Part02();
    }

    private static bool FallsInRange(int number, TicketRule rule)
    {
      return rule.Range1.From <= number && rule.Range1.To >= number || rule.Range2.From <= number && rule.Range2.To >= number;
    }

    private static (List<TicketRule> Rules, (int[] MyTicket, List<int[]> Tickets) TicketsInfo) Day16Part01(bool printOutput = true)
    {
      using (StreamReader sr = new StreamReader("Inputs/16.txt"))
      {
        List<int> invalidTicketsIndices = new List<int>();
        int ticketScanningErrorRate = 0;
        List<TicketRule> rules = new List<TicketRule>();
        int phase = 0;
        int[] myTicket = null;
        List<int[]> tickets = new List<int[]>();
        string line;
        int counter = 0;
        while ((line = sr.ReadLine()) != null)
        {
          if (line.Length == 0)
          {
            phase++;
            counter = -1;
            continue;
          }

          switch (phase)
          {
            case 0:
              string[] words = line.Split(':');
              string[] ranges = words[1].Split(' ');
              int[] range1 = ranges[1].Split('-').Select(int.Parse).ToArray();
              int[] range2 = ranges[3].Split('-').Select(int.Parse).ToArray();
              rules.Add(new TicketRule(words[0], (range1[0], range1[1]), (range2[0], range2[1])));
              break;
            case 1:
              if (counter == -1)
              {
                counter++;
                continue;
              }
              myTicket = line.Split(',').Select(int.Parse).ToArray();
              break;
            case 2:
              if (counter == -1)
              {
                counter++;
                continue;
              }

              int[] ticket = line.Split(',').Select(int.Parse).ToArray();
              tickets.Add(ticket);
              foreach (int value in ticket)
              {
                if (!rules.Any(x => FallsInRange(value, x)))
                {
                  ticketScanningErrorRate += value;
                  invalidTicketsIndices.Add(counter);
                }
              }
              break;
          }

          counter++;
        }

        if (printOutput)
        {
          Console.WriteLine($"Ticket scanning error rate is {ticketScanningErrorRate}");
        }

        return (rules, (myTicket, tickets.Where((x, i) => !invalidTicketsIndices.Contains(i)).ToList()));
      }
    }

    private static void Day16Part02()
    {
      (List<TicketRule> Rules, (int[] MyTicket, List<int[]> Tickets) TicketsInfo) info = Day16Part01(false);
      Dictionary<int, int> finalOrder = new Dictionary<int, int>();  // <indexInTicket, ruleIndex>

      while (finalOrder.Count < info.Rules.Count)
      {
        Dictionary<int, List<int>> orderCandidates = new Dictionary<int, List<int>>();  // <ruleIndex, listOfCandidates>
        for (int i = 0; i < info.Rules.Count; i++)
        {
          if (finalOrder.ContainsKey(i))
          {
            continue;
          }
          for (int j = 0; j < info.TicketsInfo.Tickets[0].Length; j++)
          {
            if (finalOrder.ContainsValue(j))
            {
              continue;
            }
            if (info.TicketsInfo.Tickets.All(x => FallsInRange(x[j], info.Rules[i])))
            {
              if (!orderCandidates.ContainsKey(i))
              {
                orderCandidates.Add(i, new List<int>());
              }
              orderCandidates[i].Add(j);
            }
          }

          if (orderCandidates[i].Count == 1)
          {
            finalOrder.Add(i, orderCandidates[i][0]);
            break;
          }
        }
      }

      ulong multiplication = 1;
      foreach (KeyValuePair<int, int> keyValuePair in finalOrder.Where(x => info.Rules[x.Key].Description.StartsWith("departure")))
      {
        multiplication *= (ulong)info.TicketsInfo.MyTicket[keyValuePair.Value];
      }

      Console.WriteLine($"Departure rules multiplication produce {multiplication}");
    }
    
    #endregion

    #region Day 17

    private static void Day17()
    {
      Day17Part01();
      Day17Part02();
    }

    private static (int active, int inactive) GetConwayCubeNeighbors(
      Dictionary<string, (int[] Center, int State)> previousState,
      Dictionary<string, (int[] Center, int State)> toAdd,
      (int[] Center, int State) cube)
    {
      (int active, int inactive) count = (0, 0);
      for (int i = -1; i <= 1; i++)
      {
        for (int j = 1; j >= -1; j--)
        {
          for (int k = -1; k <= 1; k++)
          {
            if (i == 0 && j == 0 && k == 0)
            {
              continue;
            }

            string key = $"{cube.Center[0] + k}_{cube.Center[1] + j}_{cube.Center[2] + i}";
            (int[] center, int state)? existingCube = null;
            if (previousState.ContainsKey(key))
            {
              existingCube = previousState[key];
            }

            if (existingCube == null)
            {
              if (!toAdd.ContainsKey(key))
              {
                toAdd.Add(key, (new []{cube.Center[0] + k, cube.Center[1] + j, cube.Center[2] + i}, 0));
              }
              count.inactive++;
            }
            else if (existingCube.Value.state == 0)
            {
              count.inactive++;
            }
            else if (existingCube.Value.state == 1)
            {
              count.active++;
            }
          }
        }
      }

      return count;
    }

    private static void Day17Part01()
    {
      using (StreamReader sr = new StreamReader("Inputs/17_1.txt"))
      {
        Dictionary<string, (int[] Center, int State)> cubes = new Dictionary<string, (int[] Center, int State)>();

        for (int i = 2; i >= -2; i--)
        {
          for (int j = -2; j <= 2; j++)
          {
            cubes.Add($"{j}_{i}_-1", (new[] { j, i, -1 }, 0));
          }
        }

        string[] lines = sr.ReadToEnd().Split('\n').Select(x => x.Trim()).ToArray();
        int fromY = (int) Math.Floor(lines.Length / 2.0);
        int fromX = (int)Math.Floor(lines[0].Length / 2.0);
        for (int y = fromY, iy = 0; y >= -fromY; y--, iy++)
        {
          cubes.Add($"-2_{y}_0", (new[] {-2, y, 0}, 0));
          for (int x = -fromX, ix = 0; x <= fromX; x++, ix++)
          {
            cubes.Add($"{x}_{y}_0", (new []{x, y, 0}, lines[iy][ix] == '.' ? 0 : 1));
          }
          cubes.Add($"2_{y}_0", (new[] { -2, y, 0 }, 0));
        }

        for (int i = 2; i >= -2; i--)
        {
          for (int j = -2; j <= 2; j++)
          {
            cubes.Add($"{j}_{i}_1", (new[] { j, i, 1 }, 0));
          }
        }

        for (int cycle = 0; cycle < 6; cycle++)
        {
          Dictionary<string, (int[] Center, int State)> previousState = new Dictionary<string, (int[], int)>(cubes);
          Dictionary<string, (int[] Center, int State)> toAdd = new Dictionary<string, (int[], int)>();
          foreach (KeyValuePair<string, (int[] Center, int State)> cube in previousState)
          {
            (int active, int inactive) neighborsCount = GetConwayCubeNeighbors(previousState, toAdd, cube.Value);
            if (cube.Value.State == 1 && neighborsCount.active != 2 && neighborsCount.active != 3)
            {
              cubes[cube.Key] = (cube.Value.Center, 0);
            }
            else if (cube.Value.State == 0 && neighborsCount.active == 3)
            {
              cubes[cube.Key] = (cube.Value.Center, 1);
            }
          }
          foreach (KeyValuePair<string, (int[] Center, int State)> cube in toAdd)
          {
            cubes.Add(cube.Key, cube.Value);
          }

          DrawConwayCubesState(cubes, cycle);
        }

        Console.WriteLine($"There are {cubes.Count(x => x.Value.State == 1)} active cubes after six cycles.");
      }
    }

    private static void Day17Part02()
    {

    }

    private static void DrawConwayCubesState(Dictionary<string, (int[] Center, int State)> cubes, int cycle)
    {
      int min = -2;//-1 - 2 * cycle;
      int max = 2;//1 + 2 * cycle;
      for(int z = cycle == 0 ? -1 : 1; cycle == 0 ? z <= 1 : z <= 2; z++)
      {
        Console.WriteLine($"z = {z}");
        for (int i = min; i <= max; i++)
        {
          for (int j = min; j <= max; j++)
          {
            string key = $"{i}_{j}_{z}";
            if (!cubes.ContainsKey(key))
            {
              Console.Write(".");
            }
            else
            {
              Console.Write(cubes[$"{i}_{j}_{z}"].State == 1 ? "#" : ".");
            }
          }

          Console.WriteLine();
        }

        Console.WriteLine("\n");
      }
    }

    #endregion

    #region Helpers

    public static ulong Lcm(ulong a, ulong b)
    {
      ulong num1, num2;
      if (a > b)
      {
        num1 = a; num2 = b;
      }
      else
      {
        num1 = b; num2 = a;
      }

      for (ulong i = 1; i < num2; i++)
      {
        ulong mult = num1 * i;
        if (mult % num2 == 0)
        {
          return mult;
        }
      }
      return num1 * num2;
    }

    private static void AddOrUpdateDictionary<T1, T2>(Dictionary<T1, T2> dictionary, T1 key, T2 value)
    {
      if (!dictionary.ContainsKey(key))
      {
        dictionary.Add(key, value);
      }
      else
      {
        dictionary[key] = value;
      }
    }

    #endregion
  }
}