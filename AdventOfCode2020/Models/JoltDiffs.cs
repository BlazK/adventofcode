﻿using System.Collections.Generic;

namespace AdventOfCode2020.Models
{
  public class JoltDiffs
  {
    public List<int> One { get; set; }
    public List<int> Two { get; set; }
    public List<int> Three { get; set; }

    public JoltDiffs()
    {
      One = new List<int>();
      Two = new List<int>();
      Three = new List<int>();
    }
  }
}