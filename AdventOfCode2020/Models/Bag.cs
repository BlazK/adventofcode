﻿using System.Collections.Generic;

namespace AdventOfCode2020.Models
{
  public class Bag
  {
    public string Key { get; set; }
    public List<Bag> Parents { get; set; }
    public List<(Bag, int)> Children { get; set; }

    public Bag(string key)
    {
      Key = key;
      Parents = new List<Bag>();
      Children = new List<(Bag, int)>();
    }
  }
}