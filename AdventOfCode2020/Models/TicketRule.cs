﻿namespace AdventOfCode2020.Models
{
  public class TicketRule
  {
    public string Description { get; set; }
    public (int From, int To) Range1 { get; set; }
    public (int From, int To) Range2 { get; set; }

    public TicketRule(string description, (int from, int to) range1, (int from, int to) range2)
    {
      Description = description;
      Range1 = range1;
      Range2 = range2;
    }
  }
}