﻿namespace AdventOfCode2022._02;

public class Day02
{
  private enum Option
  {
    Rock,
    Paper,
    Scissors
  }
  
  private enum Outcome
  {
    Lose,
    Draw,
    Win
  }

  private static Dictionary<string, Option> Translations = new()
  {
    { "A", Option.Rock },
    { "X", Option.Rock },
    { "B", Option.Paper },
    { "Y", Option.Paper },
    { "C", Option.Scissors },
    { "Z", Option.Scissors }
  };
  
  private static Dictionary<Option, int> Scores = new()
  {
    { Option.Rock, 1 },
    { Option.Paper, 2 },
    { Option.Scissors, 3 }
  };
  
  private static Dictionary<Option, Option> Wins = new()
  {
    { Option.Rock, Option.Scissors },
    { Option.Paper, Option.Rock },
    { Option.Scissors, Option.Paper }
  };
  
  private static Dictionary<string, Outcome> Outcomes = new()
  {
    { "X", Outcome.Lose },
    { "Y", Outcome.Draw },
    { "Z", Outcome.Win }
  };
  
  private static Dictionary<Outcome, int> OutcomeScores = new()
  {
    { Outcome.Lose, 0 },
    { Outcome.Draw, 3 },
    { Outcome.Win, 6 }
  };

  public static void Solve(int part)
  {
    int sum = 0;
   
    string? line;
    using StreamReader sr = new ("02/02.txt");
    while ((line = sr.ReadLine()) != null)
    {
      string[] moves = line.Split(" ");

      if (part == 1)
      {
        Option elf = Translations[moves[0]];
        Option me = Translations[moves[1]];
        
        sum += Scores[me];
        if (Wins[me] == elf)
        {
          sum += 6;
        }
        else if (elf == me)
        {
          sum += 3;
        } 
      }
      else
      {
        Option elf = Translations[moves[0]];
        Outcome outcome = Outcomes[moves[1]];
        
        sum += OutcomeScores[outcome];

        Option me = outcome switch
        {
          Outcome.Draw => elf,
          Outcome.Win => Wins.First(x => x.Value == elf).Key,
          _ => Wins[elf]
        };

        sum += Scores[me];
      }
    }

    Console.WriteLine($"Total sum is {sum}.");
  }
}