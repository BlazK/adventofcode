﻿namespace AdventOfCode2022._10;

public class Day10
{
  public static void Solve(int part)
  {
    int cycle = 1;
    int register = 1;

    Dictionary<int, int> values = new Dictionary<int, int>();

    string? line;
    using StreamReader sr = new("10/10.txt");
    while ((line = sr.ReadLine()) != null)
    {
      if (line.Equals("noop"))
      {
        if (part == 2)
        {
          Draw(cycle, register);
        }
        
        cycle++;

        if (part == 1)
        {
          CheckCycle(cycle, register, values);  
        }
      }
      else
      {
        if (part == 2)
        {
          Draw(cycle, register);
        }
        
        cycle++;

        if (part == 1)
        {
          CheckCycle(cycle, register, values);  
        }
        
        if (part == 2)
        {
          Draw(cycle, register);
        }
        
        string[] words = line.Split(" ");
        int value = int.Parse(words[1]);
        register += value;
        cycle++;
        
        if (part == 1)
        {
          CheckCycle(cycle, register, values);  
        }
      }
    }

    if (part == 1)
    {
      Console.WriteLine($"Total sum is {values.Sum(x => x.Key * x.Value)}."); 
    }
  }

  private static void CheckCycle(int cycle, int register, Dictionary<int, int> values)
  {
    if (cycle is 20 or 60 or 100 or 140 or 180 or 220)
    {
      values.Add(cycle, register);
    }
  }

  private static void Draw(int cycle, int register)
  {
    int row = (int)Math.Floor(cycle / 40.0);
    int col = cycle - row * 40 - 1;
    
    if ((cycle - 1) % 40 == 0)
    {
      Console.WriteLine();
    }

    Console.Write(col >= register - 1 && col <= register + 1 ? "#" : ".");
  }
}