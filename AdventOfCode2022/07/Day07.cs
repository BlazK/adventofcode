﻿namespace AdventOfCode2022._07;

class File
{
  public string Name { get; set; }
  public int Size { get; set; }

  public File(string name, int size)
  {
    Name = name;
    Size = size;
  }
}

class Directory
{
  public string Name { get; set; }
  public List<File> Files { get; set; }
  public List<Directory> Directories { get; set; }
  public Directory? ParentDirectory { get; set; }

  public Directory()
  {
    Name = "/";
    Files = new List<File>();
    Directories = new List<Directory>();
  }

  public Directory(string name, Directory parentDirectory)
  {
    Name = name;
    Files = new List<File>();
    Directories = new List<Directory>();
    ParentDirectory = parentDirectory;
  }
}

public class Day07
{
  public static void Solve(int part)
  {
    string? line;
    using StreamReader sr = new ("07/07.txt");

    Directory structure = new();
    Directory currentDirectory = structure;
    
    int threshold = 100000;
    int totalDiskSpace = 70000000;
    int updateSize = 30000000;

    while ((line = sr.ReadLine()) != null)
    {
      string[] words = line.Split(" ");
      if (words[0].Equals("$") && words[1].Equals("cd"))
      {
        switch (words[2])
        {
          case "..":
            currentDirectory = currentDirectory.ParentDirectory!;
            break;
          case "/":
            currentDirectory = structure;
            break;
          default:
            Directory? childDirectory = currentDirectory.Directories.FirstOrDefault(x => x.Name.Equals(words[2]));
            if (childDirectory == null)
            {
              childDirectory = new Directory(words[2], currentDirectory);
              currentDirectory.Directories.Add(childDirectory);
              currentDirectory = childDirectory;
            }

            break;
        }
      }
      else if (!words[0].Equals("$") && int.TryParse(words[0], out int size) && currentDirectory.Files.All(x => !x.Name.Equals(words[1])))
      {
        currentDirectory.Files.Add(new File(words[1], size));
      }
    }

    List<(string Name, int Size)> finalDirectories = new();
    CalculateSum(structure, finalDirectories);

    if (part == 1)
    {
      Console.WriteLine($"Sum of total sizes is {finalDirectories.Where(x => x.Size < threshold).Sum(x => x.Size)}");
    }
    else
    {
      int totalUsedSpace = TotalUsedSpace(structure);
      foreach ((string Name, int Size) directory in finalDirectories.OrderBy(x => x.Size))
      {
        if (totalUsedSpace + updateSize - directory.Size <= totalDiskSpace)
        {
          Console.WriteLine($"Total size of directory to delete is {directory.Size}");
          break;
        }
      } 
    }
  }

  private static int TotalUsedSpace(Directory directory)
  {
    return directory.Files.Sum(x => x.Size) + directory.Directories.Sum(TotalUsedSpace);
  }

  private static int CalculateSum(Directory directory, List<(string Name, int Size)> finalDirectories)
  {
    int sum = directory.Files.Sum(file => file.Size) + directory.Directories.Sum(childDirectory => CalculateSum(childDirectory, finalDirectories));
    finalDirectories.Add((directory.Name, sum));
    return sum;
  }
}