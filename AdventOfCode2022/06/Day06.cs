﻿namespace AdventOfCode2022._06;

public class Day06
{
  public static void Solve(int part)
  {
    string line = File.ReadAllText("06/06.txt");
    int offset = part == 1 ? 4 : 14;
    for (int i = 0; i < line.Length; i++)
    {
      if (line.Substring(i, offset).ToCharArray().Distinct().Count() == offset)
      {
        Console.WriteLine($"{i + offset} charaters need to be processed first.");
        break;
      }
    }
  }
}