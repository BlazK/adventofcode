﻿namespace AdventOfCode2022._01;

public class Day01
{
  public static void Solve(int part)
  {
    List<(int Index, int Calories)> food = new();
   
    string? line;
    int index = 0;
    int maxIndex = -1;
    
    using StreamReader sr = new ("01/01.txt");
    while ((line = sr.ReadLine()) != null)
    {
      if (int.TryParse(line, out int calories))
      {
        if (food.Count == index)
        {
          food.Add((index, 0));
        }

        food[index] = (index, food[index].Calories + calories);
      }
      else
      {
        if (maxIndex == -1 || food[index].Calories > food[maxIndex].Calories)
        {
          maxIndex = index;
        }
        
        index++;
      }
    }
    
    Console.WriteLine(part == 1 ? $"Most calories is carrying elf nr. {maxIndex} with total of {food[maxIndex]} calories." : $"Sum of calories of top three elves is {food.OrderByDescending(x => x.Calories).Skip(0).Take(3).Sum(x => x.Calories)}");
  }
}