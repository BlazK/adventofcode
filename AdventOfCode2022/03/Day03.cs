﻿namespace AdventOfCode2022._03;

public class Day03
{
  public static void Solve(int part)
  {
    int sum = 0;
    string? line;

    int groupCounter = 0;
    string[] group = new string[3];
    
    using StreamReader sr = new("03/03.txt");
    while ((line = sr.ReadLine()) != null)
    {
      if (part == 1)
      {
        int compartmentLength = line.Length / 2;
        Dictionary<int, bool[]> itemsCount = new();
        char[][] compartments = { line.Substring(0, compartmentLength).ToCharArray(), line.Substring(compartmentLength).ToCharArray() };
      
        for (int i = 0; i < compartmentLength; i++)
        {
          for (int j = 0; j < compartments.Length; j++)
          {
            int charValue = TransformChar(compartments[j][i]);
        
            if (!itemsCount.ContainsKey(charValue))
            {
              itemsCount.Add(charValue, new bool[2]);
            }
        
            itemsCount[charValue][j] = true;
          }
        }

        sum += itemsCount.Where(x => x.Value.All(y => y)).Aggregate(0, (total, next) => total + next.Key); 
      }
      else
      {
        group[groupCounter] = line;
        groupCounter = (groupCounter + 1) % 3;

        if (groupCounter == 0)
        {
          sum += line.ToCharArray().Where(x => group.All(y => y.Contains(x))).Distinct().Select(TransformChar).Sum();;
          group = new string[3];
        } 
      }
    }
    
    Console.WriteLine($"Total sum is {sum}");
  }

  private static int TransformChar(char c) => c < 97 ? c - 38 : c - 96;
}