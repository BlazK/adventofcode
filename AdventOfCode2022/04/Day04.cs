﻿namespace AdventOfCode2022._04;

public class Day04
{
  public static void Solve(int part)
  {
    int containments = 0;
    int overlaps = 0;
    
    string? line;

    using StreamReader sr = new("04/04.txt");
    while ((line = sr.ReadLine()) != null)
    {
      int[][] sections = line.Split(",").Select(x => x.Split("-").Select(int.Parse).ToArray()).Select(x => Enumerable.Range(x[0], x[1] - x[0] + 1).ToArray()).ToArray();

      if (part == 1)
      {
        int[] shorterSection = sections[0].Length < sections[1].Length ? sections[0] : sections[1];
        int[] longerSection = sections[0].Length >= sections[1].Length ? sections[0] : sections[1];
        if (shorterSection[0] >= longerSection[0] && shorterSection[^1] <= longerSection[^1])
        {
          containments++;
        } 
      }
      else
      {
        int[] sectionA = sections[0];
        int[] sectionB = sections[1];
        if (sectionA.Any(x => sectionB.Contains(x)))
        {
          overlaps++;
        }
      }
    }
    
    Console.WriteLine(part == 1 ? $"There are {containments} containments." : $"There are {overlaps} overlaps.");
  }
}