﻿namespace AdventOfCode2022._08;

public class Day08
{
  public static void Solve(int part)
  {
    string? line;
    using StreamReader sr = new ("08/08.txt");
    
    List<List<int>> grid = new();
    int highestScenicScore = int.MinValue;
    
    while ((line = sr.ReadLine()) != null)
    {
      List<int> row = line.ToCharArray().Select(x => x - '0').ToList();
      grid.Add(row);
    }

    int visible = grid[0].Count + grid[^1].Count + (grid.Count - 2) * 2;
    for (int row = 1; row < grid.Count - 1; row++)
    {
      for (int col = 1; col < grid[0].Count - 1; col++)
      {
        int tree = grid[row][col];

        List<int> treesLeftList = grid[row].Take(col).ToList();
        List<int> treesRightList = grid[row].Skip(col + 1).Take(grid[0].Count - col - 1).ToList();
        List<int> treesTopList = grid.Take(row).Select(x => x[col]).ToList();
        List<int> treesBottomList = grid.Skip(row + 1).Take(grid.Count - row - 1).Select(x => x[col]).ToList();

        if (part == 1 && (
          treesLeftList.Count(x => x < tree) == col || // left 
          treesRightList.Count(x => x < tree) == grid[row].Count - col - 1 || // right
          treesTopList.Count(x => x < tree) == row || // top
          treesBottomList.Count(x => x < tree) == grid.Count - row - 1 // bottom
        ))
        {
          visible++;
        }
        else if (part == 2)
        {
          int treesLeft = treesLeftList.Count;
          int leftHigherTreeIndex = treesLeftList.FindLastIndex(x => x >= tree);
          if (leftHigherTreeIndex > -1)
          {
            treesLeft -= leftHigherTreeIndex;
          }

          int treesRight = treesRightList.Count;
          int rightHigherTreeIndex = treesRightList.FindIndex(x => x >= tree);
          if (rightHigherTreeIndex > -1)
          {
            treesRight = rightHigherTreeIndex + 1;
          }
          
          int treesTop = treesTopList.Count;
          int topHigherTreeIndex = treesTopList.FindLastIndex(x => x >= tree);
          if (topHigherTreeIndex > -1)
          {
            treesTop -= topHigherTreeIndex;
          }
          
          int treesBottom = treesBottomList.Count;
          int bottomHigherTreeIndex = treesBottomList.FindIndex(x => x >= tree);
          if (bottomHigherTreeIndex > -1)
          {
            treesBottom = bottomHigherTreeIndex + 1;
          }
          
          int scenicScore = treesLeft * treesRight * treesTop * treesBottom;
          if (scenicScore > highestScenicScore)
          {
            highestScenicScore = scenicScore;
          }
        }
      }
    }

    Console.WriteLine(part == 1 ? $"There are {visible} trees visible." : $"Highest scenic score is {highestScenicScore}.");
  }
}