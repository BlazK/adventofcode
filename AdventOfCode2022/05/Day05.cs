﻿namespace AdventOfCode2022._05;

public class Day05
{
  public static void Solve(int part)
  {
    List<Stack<char>> stacks = new();
    bool reversed = false;
    string? line;

    using StreamReader sr = new("05/05.txt");
    while ((line = sr.ReadLine()) != null)
    {
      string[] words = line.Split(" ");
      if (line.StartsWith("move"))
      {
        if (!reversed)
        {
          for (int i = 0; i < stacks.Count; i++)
          {
            stacks[i] = new Stack<char>(stacks[i]);
          }
          reversed = true;
        }
        
        int count = int.Parse(words[1]);
        int from = int.Parse(words[3]) - 1;
        int to = int.Parse(words[5]) - 1;

        List<char> buffer = new List<char>();
        for (int i = 0; i < count; i++)
        {
          if (part == 1)
          {
            stacks[to].Push(stacks[from].Pop()); 
          }
          else
          {
            buffer.Add(stacks[from].Pop());
          }
        }

        if (part == 2)
        {
          buffer.Reverse();
          foreach (char c in buffer)
          {
            stacks[to].Push(c);
          }
        }
      }
      else if (words.Where(x => x.Length > 0).All(x => !int.TryParse(x, out _)))
      {
        if (stacks.Count == 0)
        {
          for (int i = 0; i < words.Length / 3; i++)
          {
            stacks.Add(new Stack<char>());
          }
        }
        
        for (int i = 0, index = 0; i < line.Length; i += 4, index++)
        { 
          string crate = line.Substring(i, 3);
          if (crate.StartsWith("["))
          {
            stacks[index].Push(crate[1]); 
          }
        } 
      }
    }

    Console.WriteLine($"Top crates are {string.Join(string.Empty, stacks.Where(x => x.Count > 0).Select(x => x.Peek()))}");
  }
}