﻿namespace AdventOfCode2022._09;

public class Point
{
  public int X { get; set; }
  public int Y { get; set; }

  public Point(int x, int y)
  {
    X = x;
    Y = y;
  }
}

public class Day09
{
  public static void Solve(int part)
  {
    List<string> visited = new(){"0_0"};
    int ropeLength = part == 1 ? 2 : 10;
    List<Point> rope = new ();
    for (int i = 0; i < ropeLength; i++)
    {
      rope.Add(new Point(0, 0));
    };
    
    string? line;
    using StreamReader sr = new ("09/09.txt");
    while ((line = sr.ReadLine()) != null)
    {
      string[] words = line.Split(" ");
      char direction = words[0][0];
      int value = int.Parse(words[1]);

      for (int i = 0; i < value; i++)
      {
        bool movedTheHead = false;
        for (int j = 1; j < ropeLength; j++)
        {
          if (!movedTheHead)
          {
            switch (direction)
            {
              case 'R':
                rope[j - 1].X++;
                break;
              case 'U':
                rope[j - 1].Y++;
                break;
              case 'L':
                rope[j - 1].X--;
                break;
              case 'D':
                rope[j - 1].Y--;
                break;
            }

            movedTheHead = true;
          }
          
          int distance = (int)(Math.Sqrt(Math.Pow(rope[j - 1].X - rope[j].X, 2) + Math.Pow(rope[j - 1].Y - rope[j].Y, 2)));
          if (distance > 1)
          {
            if (rope[j - 1].X == rope[j].X) // up / down
            {
              rope[j].Y = rope[j - 1].Y < rope[j].Y ? rope[j - 1].Y + 1 : rope[j - 1].Y - 1;
            }
            else if (rope[j - 1].Y == rope[j].Y) // left / right
            {
              rope[j].X = rope[j - 1].X < rope[j].X ? rope[j - 1].X + 1 : rope[j - 1].X - 1;
            }
            else
            {
              int xDiff = rope[j - 1].X > rope[j].X ? 1 : -1;
              int yDiff = rope[j - 1].Y > rope[j].Y ? 1 : -1;
              rope[j].X += xDiff;
              rope[j].Y += yDiff;
            }

            string key = $"{rope[^1].X}_{rope[^1].Y}";
            if (!visited.Contains(key))
            {
              visited.Add(key);
            }
          }
        }
      }
    }

    Console.WriteLine($"Tail visited {visited.Count} positions.");
  }
}