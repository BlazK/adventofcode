﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace AdventOfCode2015
{
  class Program
  {
    static void Main(string[] args)
    {
      //Day01Part01();
      //Day01Part02();
      //Day02Part01();
      //Day02Part02();
      //Day03Part01();
      //Day03Part02();
      //Day04();
      //Day05Part01();
      //Day05Part02();
      //Day06Part01();
      Day06Part02();
    }

    #region Day 1

    private static void Day01Part01()
    {
      int floor = 0;
      using (StreamReader sr = new StreamReader("Inputs/1.txt"))
      {
        string line = sr.ReadLine();
        foreach (char c in line)
        {
          floor = c == '(' ? floor + 1 : floor - 1;
        }
      }

      Console.WriteLine($"Final floor: {floor}");
    }

    private static void Day01Part02()
    {
      int floor = 0;
      int index = 1;
      using (StreamReader sr = new StreamReader("Inputs/1.txt"))
      {
        string line = sr.ReadLine();
        foreach (char c in line)
        {
          floor = c == '(' ? floor + 1 : floor - 1;
          if (floor == -1)
          {
            break;
          }
          index++;
        }
      }

      Console.WriteLine($"Position: {index}");
    }

    #endregion

    #region Day 2

    private static void Day02Part01()
    {
      int total = 0;
      using (StreamReader sr = new StreamReader("Inputs/2.txt"))
      {
        string line;
        while ((line = sr.ReadLine()) != null)
        {
          int[] dimensions = line.Split('x').Select(int.Parse).OrderBy(x => x).ToArray();
          int a = dimensions[0] * dimensions[1];
          int b = dimensions[0] * dimensions[2];
          int c = dimensions[1] * dimensions[2];
          total += 2 * a + 2 * b + 2 * c + a;
        }
      }

      Console.WriteLine($"Elves need {total} ft of wrapping paper.");
    }

    private static void Day02Part02()
    {
      int total = 0;
      using (StreamReader sr = new StreamReader("Inputs/2.txt"))
      {
        string line;
        while ((line = sr.ReadLine()) != null)
        {
          int[] dimensions = line.Split('x').Select(int.Parse).OrderBy(x => x).ToArray();

          int ribbon = 2 * dimensions[0] + 2 * dimensions[1];
          int bow = dimensions[0] * dimensions[1] * dimensions[2];

          total += ribbon + bow;
        }
      }

      Console.WriteLine($"Elves need {total} ft of wrapping paper.");
    }

    #endregion

    #region Day 3

    private static void Day03Part01()
    {
      (int x, int y) = (0, 0);
      Dictionary<string, int> houses = new Dictionary<string, int> { { $"{x}_{y}", 1 } };
      using (StreamReader sr = new StreamReader("Inputs/3.txt"))
      {
        string line = sr.ReadLine();
        foreach (char c in line)
        {
          switch (c)
          {
            case '^':
              y++;
              break;
            case '>':
              x++;
              break;
            case 'v':
              y--;
              break;
            default:
              x--;
              break;
          }

          string key = $"{x}_{y}";
          if (!houses.ContainsKey(key))
          {
            houses.Add(key, 1);
          }
          else
          {
            houses[key]++;
          }
        }
      }

      Console.WriteLine($"Total number of houses with at least one present: {houses.Count}.");
    }

    private static void Day03Part02()
    {
      (int x1, int y1) = (0, 0);
      (int x2, int y2) = (0, 0);
      int santaIndex = 0;
      Dictionary<string, int> houses = new Dictionary<string, int> { { $"{x1}_{y1}", 2 } };
      using (StreamReader sr = new StreamReader("Inputs/3.txt"))
      {
        string line = sr.ReadLine();
        foreach (char c in line)
        {
          switch (c)
          {
            case '^':
              if (santaIndex % 2 == 0)
              {
                y1++;
              }
              else
              {
                y2++;
              }
              break;
            case '>':
              if (santaIndex % 2 == 0)
              {
                x1++;
              }
              else
              {
                x2++;
              }
              break;
            case 'v':
              if (santaIndex % 2 == 0)
              {
                y1--;
              }
              else
              {
                y2--;
              }
              break;
            default:
              if (santaIndex % 2 == 0)
              {
                x1--;
              }
              else
              {
                x2--;
              };
              break;
          }

          string key = santaIndex == 0 ? $"{x1}_{y1}" : $"{x2}_{y2}";
          if (!houses.ContainsKey(key))
          {
            houses.Add(key, 1);
          }
          else
          {
            houses[key]++;
          }

          santaIndex = (santaIndex + 1) % 2;
        }
      }

      Console.WriteLine($"Total number of houses with at least one present: {houses.Count}.");
    }

    #endregion

    #region Day 4

    private static void Day04()
    {
      string input = "ckczppom";
      MD5 md5 = MD5.Create();

      long number = 1;
      while(true)
      {
        string hashInput = $"{input}{number}";
        byte[] hashOutput = md5.ComputeHash(Encoding.ASCII.GetBytes(hashInput));
        StringBuilder sb = new StringBuilder();
        foreach (byte b in hashOutput)
        {
          sb.Append(b.ToString("X2"));
        }

        string finalOutput = sb.ToString();
        //if (finalOutput.StartsWith("00000"))  // Part 1
        if (finalOutput.StartsWith("000000"))  // Part 2
        {
          break;
        }

        number++;
      }

      Console.WriteLine($"Lowest positive number that produces hash 00000{input} is {number}.");
    }
    
    #endregion
  
    #region Day 5

    private static void Day05Part01()
    {
      int total = 0;
      using (StreamReader sr = new StreamReader("Inputs/5.txt"))
      {
        string line;
        while ((line = sr.ReadLine()) != null)
        {
          if (line.Contains("ab") || line.Contains("cd") || line.Contains("pq") || line.Contains("xy"))
          {
            continue;
          }

          char[] chars = line.ToCharArray();
          char? previousChar = null;
          int vowelCount = 0;
          bool appearsTwice = false;
          foreach(char c in chars)
          {
            if ("aeiou".Contains(c))
            {
              vowelCount++;
            }

            if (previousChar.HasValue && previousChar.Value == c)
            {
              appearsTwice = true;
            }

            previousChar = c;
          }

          if (vowelCount >= 3 && appearsTwice)
          {
            total++;
          }
        }
      }

      Console.WriteLine($"There are {total} nice strings.");
    }

    private static void Day05Part02()
    {
      int total = 0;
      using (StreamReader sr = new StreamReader("Inputs/5.txt"))
      {
        string line;
        while ((line = sr.ReadLine()) != null)
        {
          char[] chars = line.ToCharArray();
          bool appearsTwice = false;
          bool tripletAppears = false;
          for (int i = 0, j = 0; i < chars.Length && j < chars.Length - 1; i++, j++)
          {
            if (!appearsTwice)
            {
              string sub = line.Substring(j, 2);
              for (int k = j + 2; k < line.Length - 1; k++)
              {
                if(line.Substring(k, 2).Equals(sub))
                {
                  appearsTwice = true;
                  break;
                }
              }
            }

            if (j > 0 && chars[j - 1] == chars[j + 1])
            {
              tripletAppears = true;
            }
          }

          if (appearsTwice && tripletAppears)
          {
            total++;
          }
        }
      }

      Console.WriteLine($"There are {total} nice strings.");
    }

    #endregion

    #region Day 6

    private static void Day06Part01()
    {
      int[,] lights = new int[1000, 1000];
      int total = 0;
      using (StreamReader sr = new StreamReader("Inputs/6.txt"))
      {
        string line;
        while ((line = sr.ReadLine()) != null)
        {
          string[] words = line.Split(' ');
          int[] from = words[^3].Split(',').Select(int.Parse).ToArray();
          int[] to = words[^1].Split(',').Select(int.Parse).ToArray();
          for (int i = from[0]; i <= to[0]; i++)
          {
            for (int j = from[1]; j <= to[1]; j++)
            {
              if (words[0].Equals("turn") && words[1].Equals("on") && lights[i, j] == 0)
              {
                lights[i, j] = 1;
                total++;
              }
              else if (words[0].Equals("turn") && words[1].Equals("off") && lights[i, j] == 1)
              {
                lights[i, j] = 0;
                total--;
              }
              else if (words[0].Equals("toggle"))
              {
                lights[i, j] = (lights[i, j] + 1) % 2;
                if(lights[i, j] == 0)
                {
                  total--;
                }
                else
                {
                  total++;
                }
              }
            }
          }
        }
      }

      Console.WriteLine($"There is {total} lights turned on.");
    }

    private static void Day06Part02()
    {
      int[,] lights = new int[1000, 1000];
      int totalBrightness = 0;
      using (StreamReader sr = new StreamReader("Inputs/6.txt"))
      {
        string line;
        while ((line = sr.ReadLine()) != null)
        {
          string[] words = line.Split(' ');
          int[] from = words[^3].Split(',').Select(int.Parse).ToArray();
          int[] to = words[^1].Split(',').Select(int.Parse).ToArray();
          for (int i = from[0]; i <= to[0]; i++)
          {
            for (int j = from[1]; j <= to[1]; j++)
            {
              if (words[0].Equals("turn") && words[1].Equals("on"))
              {
                lights[i, j] += 1;
                totalBrightness++;
              }
              else if (words[0].Equals("turn") && words[1].Equals("off") && lights[i, j] > 0)
              {
                lights[i, j] -= 1;
                totalBrightness--;
              }
              else if (words[0].Equals("toggle"))
              {
                lights[i, j] += 2;
                totalBrightness += 2;
              }
            }
          }
        }
      }

      Console.WriteLine($"Total brightness is {totalBrightness}.");
    }

    #endregion
  }
}
