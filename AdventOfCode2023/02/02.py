from functools import reduce


def solve(part: int):
	print(f'----- Part {part} -----')
	file_name = 'input.txt'
	f = open(file_name, 'r')
	possible_ids = []
	max_values = {'red': 12, 'green': 13, 'blue': 14}
	powers = []
	for line in f.readlines():
		cubes = {'red': 0, 'green': 0, 'blue': 0}
		is_possible = True
		split_line = line.split(': ')
		id = int(split_line[0][5:])
		for line_part in split_line[1].split('; '):
			cube_infos = line_part.split(', ')
			for cube_info in cube_infos:
				words = cube_info.split(' ')
				number = int(words[0])
				color = words[1].strip()
				if part == 1 and max_values[color] < number:
					is_possible = False
					break
				elif part == 2 and cubes[color] < number:
					cubes[color] = number
			if part == 1 and not is_possible:
				break
		if part == 1 and is_possible:
			possible_ids.append(id)
		elif part == 2:
			powers.append(reduce(lambda a, b: a * b, cubes.values()))
	print(f'Sum of possible game IDs: {sum(possible_ids)}\n' if part == 1 else f'Sum of powers is: {sum(powers)}')
	f.close()


solve(1)
solve(2)
