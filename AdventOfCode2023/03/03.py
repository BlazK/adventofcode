from typing import Dict, Tuple, List
from functools import reduce


def solve(part: int):
	print(f'----- Part {part} -----')
	file_name = 'input.txt'
	f = open(file_name, 'r')
	numbers = []
	lines = f.read().split('\n')
	number_start_index = -1
	number_end_index = -1
	adjacents = {}
	for i in range(len(lines)):
		for j in range(len(lines[i])):
			if lines[i][j].isdigit() and number_start_index == -1:
				number_start_index = j
			elif number_start_index != -1:
				if not lines[i][j].isdigit():
					number_end_index = j - 1
				elif j == len(lines[i]) - 1:
					number_end_index = j - 1 if not lines[i][j].isdigit() else j

				if number_end_index != -1:
					number = int(lines[i][number_start_index:number_end_index + 1])
					if i > 0:
						filter_start_index = number_start_index - 1 if number_start_index > 0 else 0
						filter_end_index = number_end_index + 1 if number_end_index < len(lines[i]) - 1 else len(lines[i]) - 1
						for k in range(filter_start_index, filter_end_index + 1):
							if is_symbol(lines[i - 1][k]):
								handle_adjacent(adjacents, i - 1, k, number)

					if number_start_index > 0 and not lines[i][number_start_index - 1].isdigit() and lines[i][number_start_index - 1] != '.':
						numbers.append(number)
						handle_adjacent(adjacents, i, number_start_index - 1, number)

					if number_end_index < len(lines[i]) - 1 and not lines[i][number_end_index + 1].isdigit() and lines[i][number_end_index + 1] != '.':
						numbers.append(number)
						handle_adjacent(adjacents, i, number_end_index + 1, number)

					if i < len(lines) - 1:
						filter_start_index = number_start_index - 1 if number_start_index > 0 else 0
						filter_end_index = number_end_index + 1 if number_end_index < len(lines[i]) - 1 else len(lines[i]) - 1
						for k in range(filter_start_index, filter_end_index + 1):
							if is_symbol(lines[i + 1][k]):
								handle_adjacent(adjacents, i + 1, k, number)

					number_start_index = number_end_index = -1
	if part == 2:
		adjacents = dict((k, v) for k, v in adjacents.items() if len(v) > 1)
	print(f'Sum of part numbers is: {sum(map(lambda x: sum(x), adjacents.values()))}\n' if part == 1 else f'Sum of gear ratios is: {sum(map(lambda x: reduce(lambda a, b: a * b, x), adjacents.values()))}')
	f.close()


def is_symbol(char):
	return not char.isdigit() and char != '.'


def handle_adjacent(adjacents: Dict[Tuple[int, int], List[int]], row: int, col: int, number: int):
	tup = (row, col)
	if not tup in adjacents:
		adjacents[tup] = []
	adjacents[tup].append(number)


solve(1)
solve(2)
