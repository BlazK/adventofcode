from typing import List


def solve(part: int):
	print(f'----- Part {part} -----')
	file_name = 'input.txt'
	f = open(file_name, 'r')
	result = 0
	factors = dict()
	number_of_matches_cache = dict()
	for i, line in enumerate(f.readlines()):
		numbers = line.split(': ')
		numbers_split = numbers[1].split(' | ')
		winning_numbers = list(map(lambda x: int(x), filter(lambda y: y.isdigit(), numbers_split[0].split(' '))))
		my_numbers = list(map(lambda x: int(x) if not x.endswith('\n') else int(x[:-1]), filter(lambda y: y.isdigit() or y.endswith('\n'), numbers_split[1].split(' '))))
		if part == 1:
			number_of_matches = get_number_of_matches(winning_numbers, my_numbers)
			if number_of_matches > 0:
				result = result + 1 if number_of_matches == 1 else result + 2 * number_of_matches
		else:
			iterations = 1 if i not in factors else factors[i] + 1
			for k in range(iterations):
				result += 1
				if i not in number_of_matches_cache:
					number_of_matches_cache[i] = get_number_of_matches(winning_numbers, my_numbers)
				for j in range(number_of_matches_cache[i]):
					key = i + j + 1
					if key not in factors:
						factors[key] = 0
					factors[key] = factors[key] + 1
	print(f'Cards are worth {result} points\n' if part == 1 else f'I end up with a total of {result} scratchcards')
	f.close()


def get_number_of_matches(winning_numbers: List[int], my_numbers: List[int]):
	result = 0
	for number in my_numbers:
		if number in winning_numbers:
			result += 1
	return result


solve(1)
solve(2)
