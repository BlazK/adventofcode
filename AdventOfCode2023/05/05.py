def solve(part: int):
	print(f'----- Part {part} -----')
	file_name = 'input.txt'
	f = (open(file_name, 'r'))
	seeds = []
	start_parsing = False
	parsings = []
	parsing_index = -1
	d = {}

	for line in f.readlines():
		line = line.strip()
		if len(seeds) == 0:
			seeds = [int(x) for x in line.split(': ')[1].split(' ')]
			if part == 2:
				s = []
				i = 0
				while i < len(seeds) - 1:
					for j in range(seeds[i + 1]):
						s.append(seeds[i] + j)
					i += 2
				seeds = s
			continue

		if len(line) == 0:
			start_parsing = False
			continue

		if line.endswith('map:'):
			start_parsing = True
			parsing_index += 1
			parsings.append([])
			continue

		if start_parsing:
			destination, source, length = [int(x) for x in line.split(' ')]
			parsings[parsing_index].append((destination, source, length))

	for i in range(len(seeds)):
		if seeds[i] in d:
			seeds[i] = d[seeds[i]]
		else:
			initial = seeds[i]
			for parsing in parsings:
				for p in parsing:
					min_val = p[1]
					max_val = p[1] + p[2] - 1
					if min_val <= seeds[i] <= max_val:
						seeds[i] = p[0] + seeds[i] - min_val
						break
			d[initial] = seeds[i]
	f.close()
	print(f'Lowest location number is: {min(seeds)}\n')


solve(1)
solve(2)
