from functools import reduce


def solve(part: int):
	print(f'----- Part {part} -----')
	file_name = 'input.txt'
	f = open(file_name, 'r')
	times = [int(x) for x in filter(lambda y: y.isdigit(), f.readline().strip().split(' '))] if part == 1 else [int(f.readline().strip().replace(' ', '').split(':')[1])]
	distances = [int(x) for x in filter(lambda y: y.isdigit(), f.readline().strip().split(' '))] if part == 1 else [int(f.readline().strip().replace(' ', '').split(':')[1])]
	f.close()
	beats = []
	for i in range(len(times)):
		beats.append(0)
		for speed in range(times[i]):
			time_to_move = times[i] - speed
			distance = time_to_move * speed
			if distance > distances[i]:
				beats[i] += 1
	print(f'I can beat the record in {reduce(lambda a, b: a * b, beats)} ways')


solve(1)
solve(2)
