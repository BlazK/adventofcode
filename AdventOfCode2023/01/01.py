mappings = [('one', 1), ('two', 2), ('three', 3), ('four', 4), ('five', 5), ('six', 6), ('seven', 7), ('eight', 8), ('nine', 9)]


def solve(part: int):
    print(f'----- Part {part} -----')
    file_name = 'input.txt'
    f = open(file_name, 'r')
    values = []
    for line in f.readlines():
        number = 0
        initial_add = False
        for i in range(len(line)):
            char = line[i]
            if char.isdigit():
                number = int(char)
                if not initial_add:
                    values.append(number * 10)
                    initial_add = True
            elif part == 2:
                for m_word, m_value in filter(lambda x: x[0].startswith(char), mappings):
                    if line[i:i+len(m_word)] == m_word:
                        number = m_value
                        if not initial_add:
                            values.append(m_value * 10)
                            initial_add = True
                        break
        values[-1] += number
    f.close()
    print(f'Final sum is: {sum(values)}\n')


solve(1)
solve(2)
