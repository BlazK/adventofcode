import os
import requests

day = int(input('Enter day: '))
day_dir = str(day).zfill(2)
if not os.path.isdir(day_dir):
    os.mkdir(day_dir)

src_file_name = os.path.join(day_dir, '{0}.py'.format(day_dir))
if not os.path.isfile(src_file_name):
    f = open(src_file_name, 'w')
    f.write('def solve(part: int):\n')
    f.write('\tprint(f\'----- Part {part} -----\')\n')
    f.write('\tfile_name = \'input.txt\'\n')
    f.write('\tf = open(file_name, \'r\')\n')
    f.write('\tfor line in f.readlines():\n')
    f.write('\t\t# TODO: Logic here\n')
    f.write('\n\tf.close()\n\n\nsolve(1)\n')
    f.close()

url = 'https://adventofcode.com/2023/day/{0}/input'.format(day)
cookie_file = open('cookie', 'r')
cookie = cookie_file.readline()
cookie_file.close()
response = requests.get(url, headers={'Cookie': f'session={cookie}'})
text = response.text
input_file_name = os.path.join(day_dir, 'input.txt')
if not os.path.isfile(input_file_name):
    f = open(input_file_name, 'w')
    f.write(text)
    f.close()

example_file_name = os.path.join(day_dir, 'example.txt')
if not os.path.isfile(example_file_name):
    f = open(example_file_name, 'w').close()